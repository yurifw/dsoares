# GC Medical

Breve descrição da estrutira do projeto. As principais tecnologias utilizadas são:

 - Python (backend com flask)
 - Mysql
 - Angular 2+ (frontend)
 - Nginx (server)
 - Gunicorn (server)

 A comunicação com o projeto gira em torno do Nginx, que escuta as requisição HTTP. O Nginx redireciona todas as requisição para o endereço interno (localhost) na porta 8000.
 O Gunicorn ouve essas requisição e as trata de acordo com a aplicação escrita com o Flask. As rotas disponíveis da api estão definidas no arquivo `/api/main.py`.
 Se o Flask não conhecer a rota requisitada (404), ele serve o arquivo `/api/public/index.html`  este arquivo é a aplicação escrita com angular, que deve ser compilada e movida para esta pasta. Erros de rota devem ser tratados pelo Angular

## Pastas

 - api
	 backend, contém o código da api
 - app
	 aplicativo, contém o código do aplicativo
 - scripts
	 contém scripts de utilidade para o projeto, como scripts para fazer o deploy e instalar todos os softwares que o projeto precisa para rodar
 - dashboard-app
	 frontend da parte web, é a aplicação em angular

## Principais comandos

Lista de comandos úteis:

    ssh root@[endereco]
acessa a linha de comando do servidor, será preciso ter a senha do root

---

    /etc/init.d/nginx restart
reinicia o Nginx

---

    sh [caminho do projeto]/scripts/deploy.sh
pega as ultimas alterações que foram feitos no repositorio, atualiza o código e compila tudo para ser servido. O que esse script faz é:
 1. Atualizar o repositório (descartando todas as alterações locais)
 2. Trocar o endereço da API na aplicação em Angular
 3. Compilar a aplicação em Angular
 4. Mover o resultado do passo 3 para a pasta `/api/public`

---

    nohup gunicorn -w 4 --reload main:app > ../gunicorn.log &
Faz com que o gunicorn ouça requisições na porta 8000 e use a aplicação escrita em Flask para tratar as requisições. Este comando deve ser executado na pasta `/api`. Além do Gunicorn, este comando usa o `nohup` para executar a aplicação, o nohup garante que a aplicação va continuar sendo executada mesmo depois que a sessão de ssh for encerrada. Este comando também salva o log na raiz do projeto (com o nome de gunicorn.log)
Obs.: para o nohup funcionar, é preciso encerrar a sessão de maneira limpa (com o comando exit)

---

    killall gunicorn
mata todos os processos do gunicorn
