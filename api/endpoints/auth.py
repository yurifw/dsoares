from flask import Flask, make_response
from flask import request
from . import database as db
import jwt
from jwt.exceptions import ExpiredSignatureError
import bcrypt
from datetime import datetime, timedelta
from functools import wraps
import string
import random

# acoes:
LISTAR_CLIENTES = 1
CADASTRAR_CLIENTES = 2
LISTAR_CONSISTENCIAS = 3
LISTAR_CORES = 4
LISTAR_OBRAS = 5
CADASTRAR_OBRAS = 6
LISTAR_SOLOS = 7
CADASTRAR_SONDAGEM = 8
LISTAR_SONDAGENS = 9
CADASTRAR_USUARIO = 10
VISUALIZAR_PERFIL = 11
LISTAR_UNIDADES = 12
CADASTRAR_UNIDADES = 13
ATUALIZAR_UNIDADES = 14
DELETAR_UNIDADES = 15
ATUALIZAR_CLIENTES = 16
DELETAR_CLIENTES = 17
ATUALIZAR_OBRAS = 18
DELETAR_OBRAS = 19
LISTAR_FUROS = 20
CADASTRAR_FUROS = 21
ATUALIZAR_FUROS = 22
DELETAR_FUROS = 23
ATUALIZAR_USUARIOS = 24
DELETAR_USUARIOS = 25
LISTAR_USUARIOS = 26
TROCAR_SENHA_USUARIO = 27


def generate_token(user_id, email, nome, quick_expire = False):
	private_key = None
	with open('./keys/jwtRS256.key', 'r') as priv:
		private_key = priv.read().encode()
	if quick_expire:
		expiration = datetime.utcnow() + timedelta(minutes=15)
	else:
		expiration = datetime.utcnow() + timedelta(days=30)
	payload = {"user_id":user_id, "email":email,"nome":nome, "exp":expiration}
	token = jwt.encode(payload, private_key, algorithm='RS256')
	return token.decode()

def get_token():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		senha = request.json['senha']

		cursor.execute('select * from usuario where email = %s', [email])
		usuario = cursor.fetchone()

		if usuario is None:
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})

		hash = usuario['senha']

		if bcrypt.checkpw(senha.encode(), hash.encode()):
			jwt = generate_token(usuario['id'], email, usuario['nome'])
			return make_response({"msg":'', "success":True, "data":jwt})
		else:
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def decode_token(token):
	conn, cursor = db.ready()
	try:
		public_key = None
		payload = None
		valid = False
		with open('./keys/public.key', 'r') as pub:
			public_key = pub.read().encode()
		payload = jwt.decode(token, public_key, algorithms=['RS256'], verify=True)
		valid = True
		cursor.execute("select count(*) as qtd from usuario where id = %s", [payload['user_id']])
		result = cursor.fetchone()
		if result['qtd'] == 0:
			valid = False
			payload = None
	except:
		pass
	finally:
		cursor.close()
		conn.close()

	decoded_token = {"isValid":valid, "payload":payload}
	return decoded_token

def verify_token():
	token = None
	try:
		token = request.headers['Authorization']
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})

	return make_response({"msg":'', "success":True, "data":decode_token(token)})

def trocar_senha():
	conn, cursor = db.ready()
	try:
		senha = request.json['senha']
		nova_senha = request.json['novaSenha']
		token = decode_token(request.headers['Authorization'])

		cursor.execute("select * from usuario where id = %s", [token['payload']['user_id']])
		usuario = cursor.fetchone()

		if bcrypt.checkpw(senha.encode(), usuario['senha'].encode()):
			hashed = bcrypt.hashpw(nova_senha.encode(), bcrypt.gensalt())
			cursor.execute("update usuario set senha = %s where id =%s",[hashed, token['payload']['user_id']])
			conn.commit()
			return make_response({"msg":'Senha alterada', "success":True, "data":None})
		else:
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def esqueci_senha():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		cursor.execute("select * from usuario where email = %s", [email])
		usuario = cursor.fetchone()
		if usuario is not None:
			string.printable[0:36]
			codigo = ''.join([random.choice(string.printable[0:36]) for a in range(20)])
			codigo = "123"  #para testar
			cursor.execute("update usuario set codigo_reset = %s where id = %s", [codigo, usuario['id']])
			conn.commit()
			return make_response({"msg":'Email enviado para %s' % email, "success":True, "data":None})
		else:
			return make_response({"msg":'Email não cadastrado', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def resetar_senha():
	conn, cursor = db.ready()
	try:
		codigo = request.json['codigo']
		senha = request.json['senha']

		cursor.execute("select * from usuario where codigo_reset = %s", [codigo])
		usuario = cursor.fetchone()
		if usuario is not None:
			hash = bcrypt.hashpw(senha.encode(), bcrypt.gensalt())

			cursor.execute("update usuario set codigo_reset = null, senha = %s where id = %s", [hash, usuario['id']])
			conn.commit()
			jwt = generate_token(usuario['id'], usuario['email'], usuario['nome'])
			return make_response({"msg":'Senha resetada', "success":True, "data":jwt})
		else:
			return make_response({"msg":'Código Inválido', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def requires(acao_requerida_id):  # use como decorator, acao_requerida_id eh o id da acao que o usuario deve ter permissao de realizar
	def decorator(func):
		@wraps(func)
		def decorated_func(*args, **kws):
			token = None
			try:
				token = request.headers['Authorization']
			except KeyError:
				return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
			decoded = decode_token(token)
			if not decoded['isValid']:
				return make_response({"msg":'Ação não permitida', "success":False, "data":None})

			conn, cursor = db.ready()
			try:
				acoes_usuario = []
				cursor.execute("select * from perfis_usuario where usuario_id = %s", [decoded['payload']['user_id']])
				perfis = map(lambda p: p['perfil_id'], cursor.fetchall())
				for perfil in perfis:
					cursor.execute("select * from acoes_perfil where perfil_id = %s", [perfil])
					acoes = map(lambda a: a['acao_id'], cursor.fetchall())
					acoes_usuario.extend(acoes)
			finally:
				cursor.close()
				conn.close()

			if decoded['isValid'] and acao_requerida_id in acoes_usuario:
				return func(*args, **kws)
			else:
				return make_response({"msg":'Ação não permitida', "success":False, "data":None})
		return decorated_func
	return decorator
