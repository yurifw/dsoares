from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_CLIENTES, CADASTRAR_CLIENTES, ATUALIZAR_CLIENTES, DELETAR_CLIENTES

@requires(LISTAR_CLIENTES)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from cliente";
		cursor.execute(sql)
		clientes = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":clientes})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_CLIENTES)
def insert():
	cliente = request.json
	conn, cursor = db.ready()
	try:
		sql = "insert into cliente (nome) values (%s)"
		cursor.execute(sql, [cliente['nome']])
		conn.commit()
		return make_response({"success": True, "msg":"Cliente cadastrado com sucesso!", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_CLIENTES)
def delete(id):
	conn, cursor = db.ready()
	try:
		sql = "delete from cliente where id = %s";
		cursor.execute(sql, [id])
		conn.commit()
		return make_response({"success": True, "msg":"Cliente deletado", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_CLIENTES)
def atualizar():
	conn, cursor = db.ready()
	try:
		sql = "update cliente set nome =%s where id = %s";
		cursor.execute(sql, [request.json['nome'], request.json['id'] ])
		conn.commit()
		return make_response({"success": True, "msg":"Cliente atualizado!", "data":None})
	finally:
		cursor.close()
		conn.close()
