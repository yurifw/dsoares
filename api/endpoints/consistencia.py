from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_CONSISTENCIAS

def _load_consistencias(cursor):
	sql = "select * from consistencia";
	cursor.execute(sql)
	consistencias = cursor.fetchall()
	d = {}
	for c in consistencias:
		d[c['nome']] = c['id']
	return d

@requires(LISTAR_CONSISTENCIAS)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from consistencia";
		cursor.execute(sql)
		consistencias = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":consistencias})
	finally:
		cursor.close()
		conn.close()
