from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_CORES

def _load_cores_solo(cursor):
	sql = "select * from cor_solo";
	cursor.execute(sql)
	cores_solo = cursor.fetchall()
	d = {}
	for c in cores_solo:
		d[c['nome']] = c['id']
	return d


@requires(LISTAR_CORES)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from cor_solo";
		cursor.execute(sql)
		cores_solo = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":cores_solo})
	finally:
		cursor.close()
		conn.close()
