import pymysql

MYSQL_HOST = 'localhost'
MYSQL_USER = 'prod_dsoares'
MYSQL_PASSWORD = 'nLdKprw3k'
MYSQL_DB = 'dsoares'
MYSQL_PORT = 3306


def ready():
	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(True)
	cursor = conn.cursor()
	return (conn, cursor)
