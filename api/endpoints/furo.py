from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_FUROS, CADASTRAR_FUROS, ATUALIZAR_FUROS, DELETAR_FUROS


@requires(LISTAR_FUROS)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from furo";
		cursor.execute(sql)
		furos = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":furos})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_FUROS)
def insert():
	furo = request.json
	conn, cursor = db.ready()
	try:
		sql = "insert into furo (nome) values (%s)"
		cursor.execute(sql, [furo['nome']])
		conn.commit()
		return make_response({"success": True, "msg":"Furo cadastrado com sucesso!", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_FUROS)
def delete(id):
	conn, cursor = db.ready()
	try:
		sql = "delete from furo where id = %s";
		cursor.execute(sql, [id])
		conn.commit()
		return make_response({"success": True, "msg":"Furo deletado", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_FUROS)
def atualizar():
	conn, cursor = db.ready()
	try:
		sql = "update furo set nome =%s where id = %s";
		cursor.execute(sql, [request.json['nome'], request.json['id'] ])
		conn.commit()
		return make_response({"success": True, "msg":"Furo atualizado!", "data":None})
	finally:
		cursor.close()
		conn.close()
