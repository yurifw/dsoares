from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_OBRAS, CADASTRAR_OBRAS, DELETAR_OBRAS, ATUALIZAR_OBRAS

@requires(LISTAR_OBRAS)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from obra";
		cursor.execute(sql)
		obras = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":obras})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_OBRAS)
def select_locais():
	conn, cursor = db.ready()
	try:
		sql = "select distinct local from sondagem";
		cursor.execute(sql)
		locais = []
		for row in cursor.fetchall():
			locais.append(row['local'])
		return make_response({"success": True, "msg":None, "data":locais})
	finally:
		cursor.close()
		conn.close()


@requires(CADASTRAR_OBRAS)
def insert():
	obra = request.json
	conn, cursor = db.ready()
	try:
		sql = "insert into obra (nome) values (%s)"
		cursor.execute(sql, [obra['nome']])
		conn.commit()
		return make_response({"success": True, "msg":"Obra cadastrada com sucesso!", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_OBRAS)
def delete(id):
	conn, cursor = db.ready()
	try:
		sql = "delete from obra where id = %s";
		cursor.execute(sql, [id])
		conn.commit()
		return make_response({"success": True, "msg":"Obra deletada", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_OBRAS)
def atualizar():
	conn, cursor = db.ready()
	try:
		sql = "update obra set nome =%s where id = %s";
		cursor.execute(sql, [request.json['nome'], request.json['id'] ])
		conn.commit()
		return make_response({"success": True, "msg":"Obra atualizada!", "data":None})
	finally:
		cursor.close()
		conn.close()
