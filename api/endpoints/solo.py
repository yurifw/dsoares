from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_SOLOS

def _load_solos(cursor):
	sql = "select * from solo";
	cursor.execute(sql)
	solos = cursor.fetchall()
	d = {}
	for s in solos:
		d[s['nome']] = s['id']
	return d

@requires(LISTAR_SOLOS)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from solo";
		cursor.execute(sql)
		solos = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":solos})
	finally:
		cursor.close()
		conn.close()
