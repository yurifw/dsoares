from flask import Flask, make_response
from flask import current_app
from flask import request
from datetime import datetime
import json
import base64
from . import database as db
from .consistencia import _load_consistencias
from .solo import _load_solos
from .cor_solo import _load_cores_solo
from .auth import requires, decode_token
from .auth import CADASTRAR_SONDAGEM, LISTAR_SONDAGENS

def _insert_camada0(camada0, dict_cor, dict_solo, dict_consistencia, cursor):
	cursor.execute("insert into camada0 (avanco) values (%s)", [camada0['avanco']])
	camada0_id = cursor.lastrowid
	for solo in camada0['solos']:
		cursor.execute("insert into solos_camada0 (solo_id, camada0_id) values(%s, %s)", [dict_solo[solo], camada0_id])
	for consistencia in camada0['consistencias']:
		cursor.execute("insert into consistencias_camada0 (consistencia_id, camada0_id) values(%s, %s)", [dict_consistencia[consistencia],camada0_id])
	for cor in camada0['cores']:
		cursor.execute("insert into cores_camada0 (cor_id, camada0_id) values(%s, %s)", [dict_cor[cor],camada0_id])
	return camada0_id

def _insert_etapa(etapa, dict_cor, dict_solo, dict_consistencia, cursor):
	values = [
		etapa['golpes'],
		etapa['penetracao'],
		etapa['avanco'],
		etapa['solo1']['profundidade'],
		etapa['solo2']['profundidade'],
		etapa['solo3']['profundidade']
	]
	cursor.execute("insert into etapa_camadas (golpes, penetracao, avanco, profundidade_solo1, profundidade_solo2, profundidade_solo3) values (%s, %s, %s, %s, %s, %s)", values)
	etapa_id = cursor.lastrowid
	for solo in etapa['solo1']['tipo']:
		cursor.execute("insert into solos_etapa (indice_solo, etapa_id, solo_id) values (1, %s, %s)", [etapa_id, dict_solo[solo]])
	for solo in etapa['solo2']['tipo']:
		cursor.execute("insert into solos_etapa (indice_solo, etapa_id, solo_id) values (2, %s, %s)", [etapa_id, dict_solo[solo]])
	for solo in etapa['solo3']['tipo']:
		cursor.execute("insert into solos_etapa (indice_solo, etapa_id, solo_id) values (3, %s, %s)", [etapa_id, dict_solo[solo]])
	for consistencia in etapa['consistencia']:
		cursor.execute("insert into consistencias_etapa (etapa_id, consistencia_id) values (%s, %s)", [etapa_id, dict_consistencia[consistencia]])
	for cor in etapa['cor']:
		cursor.execute("insert into cores_etapa (etapa_id, cor_id) values (%s, %s)", [etapa_id, dict_cor[cor]])
	return etapa_id

def _insert_camadas(camadas, dict_cor, dict_solo, dict_consistencia, sondagem_id, cursor):
	indice = 1
	camadas_id = []
	for camada in camadas:
		etapa1_id = _insert_etapa(camada['etapa1'], dict_cor, dict_solo, dict_consistencia, cursor)
		etapa2_id = _insert_etapa(camada['etapa2'], dict_cor, dict_solo, dict_consistencia, cursor)
		etapa3_id = _insert_etapa(camada['etapa3'], dict_cor, dict_solo, dict_consistencia, cursor)
		cursor.execute("insert into camadas (indice, profundidade, etapa1, etapa2, etapa3) values(%s, %s, %s, %s, %s)", [indice, camada['profundidade'], etapa1_id, etapa2_id, etapa3_id])
		camada_id = cursor.lastrowid
		camadas_id.append(camada_id)
		cursor.execute("insert into camadas_sondagem (sondagem_id, camada_id) values (%s, %s)", [sondagem_id, camada_id])
	return camadas_id

def _insert_lavagem(lavagem, cursor):
	periodo = lavagem['periodo1']
	cursor.execute("insert into periodo_lavagem (de, ate) values (%s, %s)", [periodo['de'], periodo['ate']])
	periodo1_id = cursor.lastrowid

	periodo = lavagem['periodo2']
	cursor.execute("insert into periodo_lavagem (de, ate) values (%s, %s)", [periodo['de'], periodo['ate']])
	periodo2_id = cursor.lastrowid

	periodo = lavagem['periodo3']
	cursor.execute("insert into periodo_lavagem (de, ate) values (%s, %s)", [periodo['de'], periodo['ate']])
	periodo3_id = cursor.lastrowid

	cursor.execute("insert into lavagem (periodo1, periodo2, periodo3) values (%s, %s, %s)", [periodo1_id, periodo2_id, periodo3_id])
	return cursor.lastrowid

def _insert_revestimento(revestimento, cursor):
	cursor.execute("insert into revestimento (inicial, final) values (%s, %s)", [revestimento['inicial'],revestimento['final']])
	return cursor.lastrowid

def _insert_nivel_agua(nivel_agua, cursor):
	cursor.execute("insert into nivel_agua (inicial, final) values (%s, %s)", [nivel_agua['inicial'],nivel_agua['final']])
	return cursor.lastrowid

def _load_sondagem(sondagem, cursor):
	cursor.execute("select * from obra where id = %s", [sondagem['obra_id']])
	obra = cursor.fetchone()
	sondagem['obra'] = obra
	del sondagem['obra_id']

	cursor.execute("select * from cliente where id = %s", [sondagem['cliente_id']])
	sondagem['cliente'] = cursor.fetchone()
	del sondagem['cliente_id']

	cursor.execute("select un.* from unidade un, usuario us, sondagem s where s.id = %s and s.sondador_id = us.id and un.id = us.unidade_id", [sondagem['agrupamento'][0]])
	sondagem['unidade'] = cursor.fetchone()
	return sondagem

def _load_etapa(etapa, cursor):
	cursor.execute("select s.* from solos_etapa e, solo s where e.etapa_id = %s and s.id = e.solo_id and indice_solo = 1", [etapa['id']])
	solo1 = {'tipos':[],'profundidade':etapa['profundidade_solo1']}
	for solo in cursor.fetchall():
		solo1['tipos'].append(solo['nome'])
	etapa['solo1'] = solo1
	del etapa['profundidade_solo1']

	cursor.execute("select s.* from solos_etapa e, solo s where e.etapa_id = %s and s.id = e.solo_id and indice_solo = 2", [etapa['id']])
	solo2 = {'tipos':[],'profundidade':etapa['profundidade_solo2']}
	for solo in cursor.fetchall():
		solo2['tipos'].append(solo['nome'])
	etapa['solo2'] = solo2
	del etapa['profundidade_solo2']

	cursor.execute("select s.* from solos_etapa e, solo s where e.etapa_id = %s and s.id = e.solo_id and indice_solo = 3", [etapa['id']])
	solo3 = {'tipos':[],'profundidade':etapa['profundidade_solo3']}
	for solo in cursor.fetchall():
		solo3['tipos'].append(solo['nome'])
	etapa['solo3'] = solo3
	del etapa['profundidade_solo3']

	cursor.execute("select c.nome from consistencias_etapa cc, consistencia c where cc.etapa_id = %s and cc.consistencia_id = c.id", [etapa['id']])
	etapa['consistencia'] = []
	for consistencias in cursor.fetchall():
		etapa['consistencia'].append(consistencias['nome'])

	cursor.execute("select c.nome from cores_etapa cc, cor_solo c where cc.etapa_id = %s and cc.cor_id = c.id", [etapa['id']])
	etapa['cor'] = []
	for cor in cursor.fetchall():
		etapa['cor'].append(cor['nome'])


	return etapa
def _load_camada0(furo, cursor):

	cursor.execute("select * from camada0 where id = %s", [furo['camada0_id']])
	camada0 = cursor.fetchone()
	cursor.execute("select s.nome from solos_camada0 c, solo s where c.camada0_id = %s and c.solo_id = s.id", [camada0['id']])
	camada0['solos'] = []
	for solo in cursor.fetchall():
		camada0['solos'].append(solo['nome'])

	cursor.execute("select c.nome from consistencias_camada0 cc, consistencia c where cc.camada0_id = %s and cc.consistencia_id = c.id", [camada0['id']])
	camada0['consistencias'] = []
	for consistencias in cursor.fetchall():
		camada0['consistencias'].append(consistencias['nome'])

	cursor.execute("select c.nome from cores_camada0 cc, cor_solo c where cc.camada0_id = %s and cc.cor_id = c.id", [camada0['id']])
	camada0['cores'] = []
	for cor in cursor.fetchall():
		camada0['cores'].append(cor['nome'])

	return camada0

def _load_camadas(furo, cursor):
	cursor.execute("select * from camadas where id in (select camada_id from camadas_sondagem where sondagem_id = %s)", [furo['id']])
	camadas = cursor.fetchall()
	for camada in camadas:
		cursor.execute("select * from etapa_camadas where id = %s",[camada['etapa1']])
		camada['etapa1'] = _load_etapa(cursor.fetchone(), cursor)
		cursor.execute("select * from etapa_camadas where id = %s",[camada['etapa2']])
		camada['etapa2'] = _load_etapa(cursor.fetchone(), cursor)
		cursor.execute("select * from etapa_camadas where id = %s",[camada['etapa3']])
		camada['etapa3'] = _load_etapa(cursor.fetchone(), cursor)
	return camadas

def _load_furo_data(furo, cursor):
	cursor.execute("select * from furo where id = %s", [furo['furo_id']])
	furo['nome_furo'] = cursor.fetchone()['nome']
	furo['camada0'] = _load_camada0(furo, cursor)
	del furo['camada0_id']
	furo['camadas'] = _load_camadas(furo, cursor)
	return furo

@requires(CADASTRAR_SONDAGEM)
def insert():
	conn, cursor = db.ready()
	try:
		sondagem = request.json
		sondador_id = decode_token(request.headers['Authorization'])['payload']['user_id']

		dict_consistencia = _load_consistencias(cursor)
		dict_solo = _load_solos(cursor)
		dict_cor = _load_cores_solo(cursor)

		camada0_id = _insert_camada0(sondagem['camada0'], dict_cor, dict_solo, dict_consistencia, cursor)

		lavagem_id = None
		if sondagem['lavagem'] is not None:
			lavagem_id = _insert_lavagem(sondagem['lavagem'], cursor)

		revestimento_id = None
		if sondagem['revestimento'] is not None:
			revestimento_id = _insert_revestimento(sondagem['revestimento'], cursor)

		nivel_agua_id = None
		if sondagem['nivelAgua'] is not None:
			nivel_agua_id = _insert_nivel_agua(sondagem['nivelAgua'], cursor)
		print(sondagem['horaInicio'])
		sql = "insert into sondagem (cliente_id, obra_id, local, furo_id, latitude, longitude, inicio, motivo_paralizacao, observacao, sondador_id, camada0_id, lavagem_id, revestimento_id, nivel_agua_id) values (%s, %s, %s, %s, %s, %s, STR_TO_DATE(%s,'%%d/%%m/%%Y %%H:%%i:%%s'), %s, %s, %s, %s, %s, %s, %s)"
		data = [
			sondagem['clienteId'],
			sondagem['obraId'],
			sondagem['local'],
			sondagem['furoId'],
			sondagem['latitude'],
			sondagem['longitude'],
			sondagem['horaInicio'],
			sondagem['motivoParalizacao'],
			sondagem['observacao'],
			sondador_id,
			camada0_id,
			lavagem_id,
			revestimento_id,
			nivel_agua_id
		]

		cursor.execute(sql, data)
		sondagem_id = cursor.lastrowid

		_insert_camadas(sondagem['camadas'], dict_cor, dict_solo, dict_consistencia, sondagem_id, cursor)


		for arquivo in sondagem['arquivos']:
			blob = base64.b64decode(arquivo['base64blob'].split('base64,')[1])
			tipo_arquivo = 1 if arquivo['base64blob'].startswith('data:image') else 0
			nome_arquivo = arquivo['nome']
			cursor.execute("insert into arquivos_sondagem (arquivo, tipo, nome, sondagem_id) values(%s,%s,%s,%s)", [blob,tipo_arquivo, nome_arquivo, sondagem_id])



		conn.commit()
		print("sondagem cadastrada")
		return make_response({"success": True, "msg":"Sondagem salva", "data":sondagem_id})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_SONDAGENS)
def download_arquivo(arquivo_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select arquivo, nome from arquivos_sondagem where id = %s", [arquivo_id])
		documentos = cursor.fetchone()
		response = make_response(documentos['arquivo'])
		response.headers.set('Content-Type', 'application/x-binary')
		response.headers.set('Content-Disposition', 'attachment', filename=documentos['nome'])
		response.headers.set('Access-Control-Expose-Headers', 'Content-Disposition')
		return response
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_SONDAGENS)
def list_all():
	conn, cursor = db.ready()
	try:
		sondagens = []
		offset = int(request.args['offset']) if 'offset' in request.args else 0
		qtd = int(request.args['qtd']) if 'qtd' in request.args else 5

		sql = "select count(*) as total from usuario u, sondagem s where s.sondador_id = u.id  group by s.furo_id, s.local, s.obra_id"
		cursor.execute(sql)
		total = len(cursor.fetchall())

		sql = "select cliente_id, local, obra_id from sondagem group by cliente_id, local, obra_id order by inicio desc limit %s, %s"
		cursor.execute(sql, [offset, qtd])
		sondagens = cursor.fetchall()

		for sondagem in sondagens:
			cursor.execute("select * from sondagem where cliente_id = %s and local = %s and obra_id = %s", [sondagem['cliente_id'], sondagem['local'], sondagem['obra_id']])
			sondagem['agrupamento'] = []  # agrupamento eh o id de todas as sondagens que estao agrupadas na query anterior
			sondagem['furos'] = []
			for row in cursor.fetchall():
				del row['cliente_id']
				del row['local']
				del row['obra_id']
				sondagem['agrupamento'].append(row['id'])
				furo = _load_furo_data(row, cursor)
				sondagem['furos'].append(furo)
			sondagem = _load_sondagem(sondagem, cursor)


		data = {'total':total, 'sondagens':sondagens}
		return make_response({"success": True, "msg":None, "data":data})
	finally:
		cursor.close()
		conn.close()
