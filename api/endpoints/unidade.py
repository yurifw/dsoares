from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import LISTAR_UNIDADES, CADASTRAR_UNIDADES, ATUALIZAR_UNIDADES, DELETAR_UNIDADES

@requires(LISTAR_UNIDADES)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = "select * from unidade";
		cursor.execute(sql)
		result = cursor.fetchall()
		return make_response({"success": True, "msg":None, "data":result})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_UNIDADES)
def insert():
	conn, cursor = db.ready()
	try:
		sql = "insert into unidade (nome, cnpj) values (%s, %s)";
		cursor.execute(sql, [request.json['nome'], request.json['cnpj'] ])
		conn.commit()
		return make_response({"success": True, "msg":"Unidade cadastrada!", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_UNIDADES)
def delete(id):
	conn, cursor = db.ready()
	try:
		sql = "delete from unidade where id = %s";
		cursor.execute(sql, [id])
		conn.commit()
		return make_response({"success": True, "msg":"Unidade deletada", "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_UNIDADES)
def atualizar():
	conn, cursor = db.ready()
	try:
		sql = "update unidade set nome =%s, cnpj =%s where id = %s";
		cursor.execute(sql, [request.json['nome'], request.json['cnpj'], request.json['id'] ])
		conn.commit()
		return make_response({"success": True, "msg":"Unidade atualizada!", "data":None})
	finally:
		cursor.close()
		conn.close()
