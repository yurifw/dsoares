from flask import Flask, make_response
from flask import request
import bcrypt
from . import database as db
from .auth import requires, decode_token
from .auth import CADASTRAR_USUARIO
import json
from base64 import b64encode
from .auth import requires, decode_token
from .auth import CADASTRAR_USUARIO, VISUALIZAR_PERFIL, ATUALIZAR_USUARIOS, DELETAR_USUARIOS, LISTAR_USUARIOS, TROCAR_SENHA_USUARIO


def _cadastrar(usuario, cursor):
	if 'foto' not in usuario or usuario['foto'] is None or len(usuario['foto']) < 1:
		with open("./public/default_foto.png","rb") as img:
			usuario['foto'] = img.read()
			usuario['mime'] = "image/png"

	password = usuario['senha']
	hashed = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

	if len(usuario['email']) < 3:
		raise AttributeError("Email inválido")

	if len(usuario['nome']) < 3:
		raise AttributeError("Nome muito curto")

	if len(usuario['senha']) < 6:
		raise AttributeError("Senha muito curta")

	if usuario['unidade_id'] is None:
		raise AttributeError("Selecione uma unidade")

	sql = "insert into usuario (email, senha, nome, unidade_id, foto, foto_mime) VALUES (%s, %s, %s, %s, %s, %s)";
	cursor.execute(sql, [usuario['email'], hashed, usuario['nome'], usuario['unidade_id'], usuario['foto'], usuario['mime']])
	usuario_id = cursor.lastrowid

	cursor.execute("insert into perfis_usuario (usuario_id, perfil_id) values (%s, %s)", [usuario_id, usuario['perfil_id']])
	return usuario_id


@requires(CADASTRAR_USUARIO)
def insert():
	conn, cursor = db.ready()
	try:
		usuario = {}
		for key in request.form:
			usuario[key] = request.form[key]
		if len(request.files) > 0:
			usuario['foto'] = request.files['foto'].read()
			usuario['mime'] = request.files['foto'].content_type
		usuario['perfil_id'] = 2
		_cadastrar(usuario,cursor)
		conn.commit()

		return make_response({"msg":"Usuário cadastrado com sucesso!", "success":True, "data":None})
	except Exception as e:
		msg = str(e)
		if "Duplicate" in msg and "email" in msg:
			msg = "Email já cadastrado"
		return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_USUARIOS)
def atualizar_usuario(id):
	conn, cursor = db.ready()
	try:
		usuario = request.json
		cursor.execute("update usuario set nome = %s, email = %s, unidade_id = %s where id = %s", [usuario['nome'], usuario['email'], usuario['unidade_id'], id])
		conn.commit()
		return make_response({"msg":"Usuário atualizado com sucesso!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_USUARIOS)
def deletar_usuario(id):
	conn, cursor = db.ready()
	try:
		cursor.execute("delete from usuario where id = %s", [id])
		conn.commit()
		return make_response({"msg":"Usuário deletado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_USUARIOS)
def select_sondadores():
	conn, cursor = db.ready()
	try:
		cursor.execute("select id, nome, email, unidade_id from usuario where id in (select usuario_id from perfis_usuario where perfil_id = 2) order by nome")
		usuarios = cursor.fetchall()
		for usuario in usuarios:
			cursor.execute("select * from unidade where id =%s", [usuario['unidade_id']])
			usuario['unidade'] = cursor.fetchone()
			del usuario['unidade_id']
			usuario['unidade_nome'] = usuario['unidade']['nome']
		return make_response({"msg":None, "success":True, "data":usuarios})
	finally:
		cursor.close()
		conn.close()

@requires(TROCAR_SENHA_USUARIO)
def trocar_senha_usuario(id):
	conn, cursor = db.ready()
	try:
		senha = request.json['nova_senha']
		hashed =  bcrypt.hashpw(senha.encode(), bcrypt.gensalt())
		cursor.execute("update usuario set senha = %s where id =%s", [hashed, id])
		conn.commit()
		return make_response({"msg":"Senha alterada!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

def info():
	token = None
	conn, cursor = db.ready()
	try:
		token = request.headers['Authorization']
		decoded = decode_token(token)
		usuario_id = decoded['payload']['user_id']
		sql = "select * from usuario where id = %s"
		cursor.execute(sql, [decoded['payload']['user_id']])
		usuario = cursor.fetchone()
		cursor.execute("select * from unidade where id = %s", [usuario['unidade_id']])
		usuario['unidade'] = cursor.fetchone()
		del usuario['codigo_reset']
		del usuario['senha']
		del usuario['unidade_id']

		if usuario['foto'] is None or len(usuario['foto']) < 2:
			with open("./public/default_foto.png","rb") as img:
				usuario['foto'] = b64encode(img.read())
			usuario['foto_mime'] = "image/png"
		else:
			usuario['foto'] = b64encode(usuario['foto'])
		usuario['foto'] = usuario['foto'].decode("utf-8")
		return make_response({"msg":None, "success":True, "data":usuario})
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def atualizar_perfil():
	token = None
	conn, cursor = db.ready()
	try:
		token = request.headers['Authorization']
		decoded = decode_token(token)
		usuario_id = decoded['payload']['user_id']
		if request.files['foto']:
			foto = request.files['foto'].read()
			mime = request.files['foto'].content_type
			cursor.execute("update usuario set foto = %s, foto_mime = %s where id = %s", [foto, mime, usuario_id])
		if request.form['nome']:
			cursor.execute("update usuario set nome = %s where id = %s", [request.form['nome'], usuario_id])
		conn.commit()
		return make_response({"msg":"Atualização salva!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()
