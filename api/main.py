import os

from flask import Flask
from flask import render_template
from flask_cors import CORS
from flask import request

import endpoints.sondagem as sondagem
import endpoints.auth as auth
import endpoints.cliente as cliente
import endpoints.obra as obra
import endpoints.solo as solo
import endpoints.consistencia as consistencia
import endpoints.cor_solo as cor_solo
import endpoints.usuario as usuario
import endpoints.unidade as unidade
import endpoints.furo as furo

app = Flask(__name__, static_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
CORS(app)



app.add_url_rule('/api/sondagem', 'sondagem_in', sondagem.insert, methods=['POST'])
app.add_url_rule('/api/sondagem', 's_list_un', sondagem.list_all, methods=['GET'])
app.add_url_rule('/api/sondagem/download/arquivo/<int:arquivo_id>', 'sondagem_dow', sondagem.download_arquivo, methods=['GET'])
app.add_url_rule('/api/login', 'jwt_get', auth.get_token, methods=['POST'])
app.add_url_rule('/api/login', 'senha', auth.trocar_senha, methods=['PATCH'])
app.add_url_rule('/api/login/verify', 'jwt_check', auth.verify_token, methods=['GET'])
app.add_url_rule('/api/esqueci-senha', 's_e', auth.esqueci_senha, methods=['POST'])
app.add_url_rule('/api/resetar-senha', 's_r', auth.resetar_senha, methods=['PATCH'])
app.add_url_rule('/api/cliente', 's_c', cliente.select_all, methods=['GET'])
app.add_url_rule('/api/cliente', 's_p', cliente.insert, methods=['POST'])
app.add_url_rule('/api/cliente', 's_a', cliente.atualizar, methods=['PATCH'])
app.add_url_rule('/api/cliente/<int:id>', 'c_d', cliente.delete, methods=['DELETE'])
app.add_url_rule('/api/obra', 'o_c', obra.select_all, methods=['GET'])
app.add_url_rule('/api/obra', 'o_p', obra.insert, methods=['POST'])
app.add_url_rule('/api/obra', 'o_a', obra.atualizar, methods=['PATCH'])
app.add_url_rule('/api/obra/locais', 'o_l', obra.select_locais, methods=['GET'])
app.add_url_rule('/api/obra/<int:id>', 'o_d', obra.delete, methods=['DELETE'])
app.add_url_rule('/api/solo', 'sa', solo.select_all, methods=['GET'])
app.add_url_rule('/api/consistencia', 'ca', consistencia.select_all, methods=['GET'])
app.add_url_rule('/api/cor-solo', 'cs', cor_solo.select_all, methods=['GET'])
app.add_url_rule('/api/usuario', 'iu', usuario.insert, methods=['POST'])
app.add_url_rule('/api/usuario', 'ui', usuario.info, methods=['GET'])
app.add_url_rule('/api/usuario/sondadores', 'uia', usuario.select_sondadores, methods=['GET'])
app.add_url_rule('/api/usuario', 'ua', usuario.atualizar_perfil, methods=['PATCH'])
app.add_url_rule('/api/usuario/<int:id>', 'uaid', usuario.atualizar_usuario, methods=['PATCH'])
app.add_url_rule('/api/usuario/senha/<int:id>', 'usid', usuario.trocar_senha_usuario, methods=['PATCH'])
app.add_url_rule('/api/usuario/<int:id>', 'udd', usuario.deletar_usuario, methods=['DELETE'])
app.add_url_rule('/api/unidade', 'u_p', unidade.insert, methods=['POST'])
app.add_url_rule('/api/unidade', 'u_a', unidade.atualizar, methods=['PATCH'])
app.add_url_rule('/api/unidade', 'u_l', unidade.select_all, methods=['GET'])
app.add_url_rule('/api/unidade/<int:id>', 'u_d', unidade.delete, methods=['DELETE'])
app.add_url_rule('/api/furo', 'f_p', furo.insert, methods=['POST'])
app.add_url_rule('/api/furo', 'f_a', furo.atualizar, methods=['PATCH'])
app.add_url_rule('/api/furo', 'f_l', furo.select_all, methods=['GET'])
app.add_url_rule('/api/furo/<int:id>', 'f_d', furo.delete, methods=['DELETE'])

# sends 404 to index so angular can handle them
@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:] if len(request.path[1:]) > 0 else "index.html"
	if "." not in requested_file:
		requested_file = "index.html"
	return app.send_static_file(requested_file)
