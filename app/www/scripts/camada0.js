if (getSondagem()['camada0']) {
	location.href = "camadas.html"
}

_Solos = [];
_Consistencias = "";
_Cores = [];

getConsistencias((consistencias) => {
	var i = 0;
	for (i = 0; i < consistencias.length; i++) {
		var o = new Option(consistencias[i].nome, consistencias[i].nome);
		$(o).html(consistencias[i].nome);
		$("#consistencia").append(o);
	}
})


getSolos((solos) => {
	array = solos.map(c => { return c.nome })
	var soloBloodhound = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: array
	});

	_Solos = array;

	$('#multiSoloBloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
})

getCoresSolos((cores) => {
	array = cores.map(c => { return c.nome })
	var corSoloBloodhound = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: array
	});

	_Cores = array;

	$('#multiCorBloodhound .typeahead').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'states',
		source: corSoloBloodhound
	});

})

function continuar() {
	avanco = $("#tradoSelect").val()
	avanco = avanco == "outro" ? $("#SpecifyAdvanceInput").val() : avanco

	solos = getMultiTagArray("multiSoloBloodhound")
	_Consistencias = $("#consistencia").val()
	cores = getMultiTagArray("multiCorBloodhound")

	if (solos.length == 0) {
		toastr.warning("Escolha ao menos um solo")
		return
	}
	console.log(_Consistencias);
	if (_Consistencias == "") {
		toastr.warning("Escolha ao menos uma consistência")
		return
	}
	if (cores.length == 0) {
		toastr.warning("Escolha ao menos uma cor")
		return
	}
	setCamada0(solos, _Consistencias, cores, avanco)
	location.href = "camadas.html"

}
