if ('observacao' in getSondagem()) {
	location.href = "menu.html"
}

var displayedImages = {}
var fileNames = {}

_Solos = [];
_Consistencias = [];
_Cores = [];

loadArquivos()

$('#solo1Etapa1 > input').tagsinput({
	maxTags: 1
});

$('#solo2Etapa1 > input').tagsinput({
	maxTags: 1
});

$('#solo3Etapa1 > input').tagsinput({
	maxTags: 1
});

$('#solo1Etapa2 > input').tagsinput({
	maxTags: 1
});

$('#solo2Etapa2 > input').tagsinput({
	maxTags: 1
});

$('#solo3Etapa2 > input').tagsinput({
	maxTags: 1
});

$('#solo1Etapa3 > input').tagsinput({
	maxTags: 1
});

$('#solo2Etapa3 > input').tagsinput({
	maxTags: 1
});

$('#solo3Etapa3 > input').tagsinput({
	maxTags: 1
});

getSolos((solos) => {
	array = solos.map(c => { return c.nome })
	var soloBloodhound = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: array
	});

	_Solos = array;

	$('#solo1Etapa1Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo2Etapa1Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo3Etapa1Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);


	$('#solo1Etapa2Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo2Etapa2Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo3Etapa2Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);


	$('#solo1Etapa3Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo2Etapa3Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
	$('#solo3Etapa3Bloodhound .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: soloBloodhound }
	);
})


getConsistencias((consistencias) => {
	var i = 0;
	for (i = 0; i < consistencias.length; i++) {
		var o = new Option(consistencias[i].nome, consistencias[i].nome);
		$(o).html(consistencias[i].nome);
		$("#consistenciaCamada1").append(o);
	}
})

getCoresSolos((cores) => {
	array = cores.map(c => { return c.nome })
	var corSoloBloodhound = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: array
	});

	_Cores = array;

	$('#corEtapa1 .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: corSoloBloodhound }
	);
	$('#corEtapa2 .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: corSoloBloodhound }
	);
	$('#corEtapa3 .typeahead').typeahead(
		{ hint: true, highlight: true, minLength: 1 },
		{ name: 'states', source: corSoloBloodhound }
	);

})

function confirmarGeral() {

	houveLavagem = $("#checkWash").hasClass("modal_content_checkbox--checked")
	usouRevestimento = $("#checkCoating").hasClass("modal_content_checkbox--checked")

	if (houveLavagem) {
		["#De1-10min", "#A1-10min", "#De2-10min", "#A2-10min", "#De3-10min", "#A3-10min"].forEach((id) => {
			if (Number.isNaN(id)) {
				toastr.warning("Preencha todos campos para continuar")
				return
			}
		})

		lavagem = {
			"periodo1": { "de": parseInt($("#De1-10min").val()), "ate": parseInt($("#A1-10min").val()) },
			"periodo2": { "de": parseInt($("#De2-10min").val()), "ate": parseInt($("#A2-10min").val()) },
			"periodo3": { "de": parseInt($("#De3-10min").val()), "ate": parseInt($("#A3-10min").val()) }
		}
		adicionarLavagem(lavagem)
	} else {
		adicionarLavagem(null)
	}

	if (usouRevestimento) {
		inicial = parseInt($("#initialFootage").val())
		final = parseInt($("#finalFootage").val())
		if (Number.isNaN(inicial) || Number.isNaN(final)) {
			toastr.warning("Preencha os campos inicial e final do revestimento")
			return
		}

		revestimento = {
			"inicial": inicial,
			"final": final,
		}
		adicionarRevestimento(revestimento)
	} else {
		adicionarRevestimento(null)
	}

	closemodalGeneral()
}

function confirmarNivelAgua() {
	seco = $("#checkDry").hasClass("modal_content_checkbox--checked")
	if (seco) {
		adicionarNivelAgua(null)
	} else {
		if (Number.isNaN(parseInt($("#startLevel").val())) || Number.isNaN(parseInt($("#finalLevel").val()))) {
			toastr.warning("Preencha o nível de água inicial e o final")
			return
		}

		nivelAgua = {
			"inicial": parseInt($("#startLevel").val()),
			"final": parseInt($("#finalLevel").val())
		}
		adicionarNivelAgua(nivelAgua)
	}
	closemodalWaterLevel()
}

function getCamadaIndex() {
	sondagem = getSondagem()
	var camada = 0
	if (sondagem.camadas) {
		camada = sondagem.camadas.length + 1
	} else {
		camada = 1
	}
	$("#nomeCamada").html("CAMADA " + camada)
	setTimeout(() => {
		if (camada > 1){
			ultimaCamada = getSondagem()['camadas'].pop()
			propagarEtapa(ultimaCamada['etapa3'], [1,2,3])
		}
	}, 2000);

}

function getEtapa(index){
	if(index == 1){
		deep = [1,2,3]
	}
	if(index == 2){
		deep = [4,5,6]
	}
	if(index == 3){
		deep = [7,8,9]
	}

	etapa = {
		"golpes": parseInt($("#hits"+index).val()),
		"penetracao": parseInt($("#penetration"+index).val()),
		"solo1": {
			"tipo": getMultiTagArray("solo1Etapa"+index+"Bloodhound"),
			"profundidade": parseInt($("#deepness"+deep[0]).val())
		},
		"solo2": {
			"tipo": getMultiTagArray("solo2Etapa"+index+"Bloodhound"),
			"profundidade": parseInt($("#deepness"+deep[1]).val())
		},
		"solo3": {
			"tipo": getMultiTagArray("solo3Etapa"+index+"Bloodhound"),
			"profundidade": parseInt($("#deepness"+deep[2]).val())
		},
		"consistencia": $("#consistenciaCamada1").val(),
		"cor": getMultiTagArray("corEtapa"+index+"Bloodhound"),
		"avanco": $("#trado"+index).val() == "outro" ? $("#EspecificarAvanco"+index).val() : $("#trado"+index).val()
	}
	return etapa

}

function btnFinalizar() {
	camadaFinal = $("#checkFinal").hasClass('container_box_itens_checkbox--checked')
	etapa1 = getEtapa(1)
	// etapa2 = getEtapa(2)
	// etapa3 = getEtapa(3)
	camada = {
		"profundidade": parseInt($("#finalDeepness").val()),
		"etapa1": etapa1,
		// "etapa2": etapa2,
		// "etapa3": etapa3
	}
	if (!(validaEtapa(etapa1)) || $("#finalDeepness").val() == '') {
		return
	}

	if (camadaFinal) {
		sondagem = getSondagem()
		if (!('lavagem' in sondagem) || !('revestimento' in sondagem)) {
			toastr.warning("Confirme os dados da aba Geral")
			return
		}
		if (!('nivelAgua' in sondagem)) {
			toastr.warning("Confirme os dados da aba Nv. de Água")
			return
		}
		camadaFinalCache(camada)
		openFinalModel()

	} else {
		adicionarCamada(camada)
		document.location.reload(true);
	}
}

function validaEtapa(etapa) {
	if (etapa.golpes < 0) {
		toastr.warning("O número de golpes não pode ser negativo")
		return false
	}
	if (Number.isNaN(etapa.penetracao) || etapa.penetracao < 0) {
		toastr.warning("Digite um número válido para a penetração")
		return false
	}
	if (etapa.solo1.tipo.length == 0) {
		toastr.warning("O campo solo não pode ficar vazio")
		return false
	}
	if (Number.isNaN(etapa.solo1.profundidade)) {
		toastr.warning("Digite um número válido para a profundidade do solo")
		return false
	}
	if (etapa.consistencia == "") {
		toastr.warning("O campo consistência não pode ficar vazio")
		return false
	}
	if (etapa.cor == 0) {
		toastr.warning("O campo cor não pode ficar vazio")
		return false
	}
	return true
}

function loadArquivos() {
	sondagem = getSondagem()
	if (sondagem.arquivos) {
		sondagem['arquivos'].forEach((arquivo) => {
			index = parseInt(Math.random() * 10000000000)
			displayedImages[index] = arquivo.base64blob
			fileNames[index] = arquivo.nome
			showThumbnail(index, arquivo.base64blob)
		});
	}
}

function showThumbnail(index, image) {
	var thumbnailFather = $("#thumbnailContainer")

	if (image.startsWith("data:image")) {
		var htmlTemplate = '<span class="imageBoxWidth"><div id="' + index + '" class="imageConfig"></div> <span class="closeIconImage" onclick=""> <i class="fa fa-times-circle"></i></span> </span>'
		thumbnailFather.prepend(htmlTemplate)
		$("#" + index).css('background-image', "url('" + image + "')")

	} else {
		var htmlTemplate = '<span id="' + index + '" class="iconWidth fa-stack fa-2x"><i class="documentColor fa fa-square fa-stack-2x"></i><i class="iconSize--large fa fa-file-alt fa-stack-1x fa-inverse"></i><span class="closeIcon">				<i class="fa fa-times-circle"></i></span></span>'
		thumbnailFather.prepend(htmlTemplate)

	}
	$("#" + index).click((event) => {
		// index = $(event.target).attr('id')
		delete displayedImages[index]
		delete fileNames[index]
		$(event.target).parent().remove()
	})


}

function imageSelected(files) {

	var fr = new FileReader();
	fr.onload = function (e) {
		index = parseInt(Math.random() * 10000000000)
		displayedImages[index] = this.result
		fileNames[index] = files[0].name
		showThumbnail(index, this.result)
	};
	fr.readAsDataURL(files[0]);
}

function adicionarImagens() {
	// console.log(Object.entries(displayedImages))
	imagemArray = []
	for (const index in displayedImages) {
		imagemArray.push({ "base64blob": displayedImages[index], "nome": fileNames[index] })
	}
	setArquivos(imagemArray)
	closemodalMap()
}

function selectImage() {
	$("#imgSelector").click()
}

function selectFile() {
	$("#fileSelector").click()
}

function propagarEtapa(etapa, para){

	ultimoGolpe = etapa.golpes
	para.forEach(etapaIndex => {
		$("#hits"+etapaIndex).val(ultimoGolpe)
	})

	ultimaPenetracao = etapa.penetracao
	para.forEach(etapaIndex => {
		$("#penetration"+etapaIndex).val(ultimaPenetracao)
	})

	ultimoAvanco = etapa.avanco
	avancos = Array.from($('#trado1 option').map((index, option) => option.value))
	para.forEach(etapaIndex => {
		if(avancos.includes(ultimoAvanco)){
			$('#trado'+etapaIndex).val(ultimoAvanco)
		} else {
			$('#trado'+etapaIndex).val("outro")
			EnableSpecifyAdvance('trado'+etapaIndex, 'labelAvanco'+etapaIndex, 'EspecificarAvanco'+etapaIndex)
			$('#EspecificarAvanco'+etapaIndex).val(ultimoAvanco)
		}
	})


	ultimaConsistencia = etapa.consistencia
	para.forEach(etapaIndex => {
		setMultiTagArray("consistenciaEtapa"+etapaIndex,ultimaConsistencia)
	})

	ultimaCor = etapa.cor
	para.forEach(etapaIndex => {
		setMultiTagArray("corEtapa"+etapaIndex,ultimaCor)
	})

	ultimoSolo = null
	if (etapa.solo1.tipo.length){
		ultimoSolo = etapa.solo1
	}
	if (etapa.solo2.tipo.length) {
		ultimoSolo = etapa.solo2
	}
	if(etapa.solo3.tipo.length){
		ultimoSolo = etapa.solo3
	}

	etapaToDeep = [1,4,7]
	para.forEach(etapaIndex => {
		setMultiTagArray("solo1Etapa"+etapaIndex, ultimoSolo.tipo)
		$("#deepness"+etapaToDeep[etapaIndex-1]).val(ultimoSolo.profundidade)
	})
}

function etapaFinalizada(indice){
	if (indice == 1){
		propagarEtapa(getEtapa(indice), [2, 3])
	}
	if (indice == 2){
		propagarEtapa(getEtapa(indice), [3])
	}
}
getCamadaIndex()

$("#trado1").change(()=>{
	etapaFinalizada(1)
})

$("#EspecificarAvanco1").keypress(()=>{
	etapaFinalizada(1)
})

$("#trado2").change(()=>{
	etapaFinalizada(2)
})

$("#EspecificarAvanco2").keypress(()=>{
	etapaFinalizada(2)
})
