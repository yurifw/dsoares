function updateData(){
	getClientes()
	getObras()
	getSolos()
	getConsistencias()
	getCoresSolos()
	getConsistencias()
	getFuros()
	getLocais()
}

function limparDadosOffline(){
	localStorage.removeItem('solos')
	localStorage.removeItem('obras')
	localStorage.removeItem('coresSolo')
	localStorage.removeItem('consistencia')
	localStorage.removeItem('clientes')
	localStorage.removeItem('furos')
	localStorage.removeItem('locais')
}

function offlineDataUpdated(){
	var updated = true
	updated = updated && localStorage.getItem('solos') && localStorage.getItem('solos').length > 0
	updated = updated && localStorage.getItem('obras') && localStorage.getItem('obras').length > 0
	updated = updated && localStorage.getItem('coresSolo') && localStorage.getItem('coresSolo').length > 0
	updated = updated && localStorage.getItem('consistencia') && localStorage.getItem('consistencia').length > 0
	updated = updated && localStorage.getItem('clientes') && localStorage.getItem('clientes').length > 0
	updated = updated && localStorage.getItem('furos') && localStorage.getItem('furos').length > 0
	updated = updated && localStorage.getItem('locais') && localStorage.getItem('locais').length > 0
	return updated
}

function getClientes(callback){
	api(GET, "cliente",null, (clientes) =>{
		localStorage.setItem("clientes", JSON.stringify(clientes))
		if (callback) {
			callback(clientes)
		}
	},
	()=>{
		if (callback) {
			callback(JSON.parse(localStorage.getItem("clientes")))
		}
	})
}

function getObras(callback){
	api(GET, "obra",null, (obras) =>{
		localStorage.setItem("obras", JSON.stringify(obras))
		if(callback){
			callback(obras)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("obras")))
		}
	})
}

function getSolos(callback){
	api(GET, "solo",null, (solo) =>{
		localStorage.setItem("solos", JSON.stringify(solo))
		if(callback){
			callback(solo)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("solos")))
		}
	})
}

function getCoresSolos(callback){
	api(GET, "cor-solo",null, (coresSolo) =>{
		localStorage.setItem("coresSolo", JSON.stringify(coresSolo))
		if(callback){
			callback(coresSolo)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("coresSolo")))
		}
	})
}

function getConsistencias(callback){
	api(GET, "consistencia", null, (consistencia) =>{
		localStorage.setItem("consistencia", JSON.stringify(consistencia))
		if(callback){
			callback(consistencia)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("consistencia")))
		}
	})
}

function getFuros(callback){
	api(GET, "furo", null, (furos) =>{
		localStorage.setItem("furos", JSON.stringify(furos))
		if(callback){
			callback(furos)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("furos")))
		}
	})
}

function getLocais(callback){
	api(GET, "obra/locais", null, (locais) =>{
		localStorage.setItem("locais", JSON.stringify(locais))
		if(callback){
			callback(locais)
		}
	},
	()=>{
		if (callback){
			callback(JSON.parse(localStorage.getItem("locais")))
		}
	})
}

function getJWT(){
	return localStorage.getItem("jwt")
}

function getSondagem(){
	return JSON.parse(localStorage.getItem("sondagem"))
}

function setInicioSondagem(clienteId, obraId, numeroFuro, local, latitude, longitude){
	dt = new Date()
	data = dt.getUTCDate().toString().padStart(2,"0")+"/"
	data += dt.getUTCMonth().toString().padStart(2,"0")+"/"
	data += dt.getUTCFullYear()+" "
	data += dt.getUTCHours().toString().padStart(2,"0")+":"
	data += dt.getUTCMinutes().toString().padStart(2,"0")+":"
	data += dt.getUTCSeconds().toString().padStart(2,"0")

	sondagem = {
		clienteId: clienteId,
		obraId: obraId,
		furoId: numeroFuro,
		latitude: latitude,
		longitude: longitude,
		local: local,
		horaInicio: data
	}
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function setArquivos(arquivo){
	sondagem = getSondagem()
	sondagem['arquivos'] = arquivo
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function setCamada0(solos, consistencias, cores, avanco){
	sondagem = getSondagem()
	sondagem.camada0 = {}
	sondagem.camada0.solos = solos
	sondagem.camada0.consistencias = consistencias
	sondagem.camada0.cores = cores
	sondagem.camada0.avanco = avanco
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function camadaFinalCache(camada){
	sessionStorage.setItem('camada-final', JSON.stringify(camada))
}

function adicionarCamada(camada){
	sondagem = getSondagem()
	if(sondagem['camadas']){
		sondagem['camadas'].push(camada)
	} else {
		sondagem['camadas'] = []
		sondagem['camadas'].push(camada)
	}
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function adicionarLavagem(lavagem){
	sondagem = getSondagem()
	sondagem['lavagem'] = lavagem
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function adicionarRevestimento(revestimento){
	sondagem = getSondagem()
	sondagem['revestimento'] = revestimento
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function adicionarNivelAgua(nivelAgua){
	sondagem = getSondagem()
	sondagem['nivelAgua'] = nivelAgua
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function adicionarMotivoParalizacao(motivo){
	sondagem = getSondagem()
	sondagem['motivoParalizacao'] = motivo
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function adicionarObservacao(obs){
	sondagem = getSondagem()
	sondagem['observacao'] = obs
	camadaFinal = JSON.parse(sessionStorage.getItem("camada-final"))
	if(sondagem['camadas']){
		sondagem['camadas'].push(camadaFinal)
	} else {
		sondagem['camadas'] = [camadaFinal]
	}
	localStorage.setItem("sondagem", JSON.stringify(sondagem))
}

function limparSondagem(){
	localStorage.removeItem("sondagem")
}
