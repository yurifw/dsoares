if(getSondagem()['mapa']){
	location.href = "camada0.html"
}

function selectImage(){
	$("#imgSelector").click()
}

var displayedImages = {}

function showThumbnail(index, image){
	var thumbnailFather = $("#thumbnailContainer")
	var htmlTemplate = '<span class="imageBoxWidth"><div id="'+index+'" class="imageConfig"></div> <span class="closeIconImage" onclick=""> <i class="fa fa-times-circle"></i></span> </span>'
	thumbnailFather.prepend(htmlTemplate)
	$("#"+index).css('background-image', "url('"+image+"')")

	$("#"+index).click((event)=>{
		index = $(event.target).attr('id')
		delete displayedImages[index]
		$(event.target).parent().remove()
	})

}

function imageSelected(files){

	var fr=new FileReader();
	fr.onload = function(e) {
		// console.log(this.result)
		index = parseInt(Math.random()*10000000000)
		displayedImages[index] = this.result
		showThumbnail(index, this.result)
	};
	fr.readAsDataURL(files[0]);
}

function adicionarImagens(){
	// console.log(Object.entries(displayedImages))
	imagemArray = []
	for (const index in displayedImages) {
		imagemArray.push(displayedImages[index])
	}
	setMapaImages(imagemArray)
	location.href = "camada0.html"
}
