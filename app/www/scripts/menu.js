sondagem = getSondagem()
if (sondagem){
	if('observacao' in sondagem){
		$("#divContinuar").hide()
	} else {
		$("#divEnviar").hide()
	}
} else {
	$("#divEnviar").hide()
}

function continuar(){
	location.href = "telaInicial.html"
}

function reset(){
	sondagem = getSondagem()
	delete sondagem.observacao
	localStorage.setItem('sondagem',JSON.stringify(sondagem))
}

function enviar (){
	toastr.info("Enviando sondagem, por favor aguarde")

	api(POST,"sondagem",sondagem,(success)=>{
		limparSondagem()
		location.href = "menu.html"
	}, (error)=>{
		toastr.error("Não foi possível salvar a sondagem")
		console.log(error)
	})
}

function atualizar(){
	toastr.info("Atualizando dados, aguarde até receber a confirmação")
	limparDadosOffline()
	updateData()

	var check = function(){
		updated = offlineDataUpdated()
		console.log(updated)
	    if(updated){
	        toastr.success("Dados atualizados !!!")
	    }
	    else {
	        setTimeout(check, 1000); // check again in a second
	    }
	}

	check()
}

function logout(){
	localStorage.removeItem("jwt")
	location.href = "index.html"
}
