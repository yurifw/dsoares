window.onload = function () {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-full-width",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
};

GET = 0
POST = 1
PATCH = 2
DELETE = 3
server = "http://107.152.39.115/api/"

function api(method, endpoint, body, dataCallback, failCallBack){
	server = "http://107.152.39.115/api/"
	jwt = localStorage.getItem("jwt")||""

	if(method == GET){method = "GET"}
	if(method == POST){method = "POST"}
	if(method == PATCH){method = "PATCH"}
	if(method == DELETE){method = "DELETE"}

	$.ajax({
		method: method,
		url: server+endpoint,
		contentType: "application/json; charset=UTF-8",
		headers: {
			"Authorization":jwt
		},
		data: JSON.stringify(body),
		success: function( resp ) {
			if(resp.msg){
				if(resp.success){
					toastr.success(resp.msg)
				} else {
					toastr.error(resp.msg)
				}
			}
			if(resp.data){
				dataCallback( resp.data );
			}
		},
		error: function (xhr, status, error){
			alert(xhr)
			alert(status)
			alert(error)
			failCallBack()
		}
	})
}
//Método para fazer o login
function performLogin(){
	api(POST, "login", {"email":$("#username").val(), "senha":$("#password").val()}, (data) =>{
		localStorage.setItem("jwt",data)
		location.href = "telaInicial.html"
	})
}

function checkFinal() {
  var checkbox = document.getElementById("checkFinal");
  var button = document.getElementById("buttonFinal");
  var modal = document.getElementById("modalMsg");
  if (!checkbox.classList.contains('container_box_itens_checkbox--checked')) {
    modal.style.display = 'block';
    checkbox.classList.toggle("container_box_itens_checkbox--checked");
    button.classList.toggle("container_box_item-col2_btn--alternative");
  }
  else {
    checkbox.classList.toggle("container_box_itens_checkbox--checked");
    button.classList.toggle("container_box_item-col2_btn--alternative");
  }
}

function openFinalModel() {
  var modal = document.getElementById("modalParalysis");
  modal.style.display = 'block';
}

function closeModal() {
  var modal = document.getElementById("modalMsg")
  modal.style.display = 'none';
}

function verificarMotivo() {
  var x = document.getElementById("reasons").value;
  var y = document.getElementById("otherReason");
  if (x == "Outro") {
    y.style.display = 'block';
  }
  else {
    y.style.display = 'none';
  }
}

function removeTags(id){
	if(id =="solo1Etapa1"){
		$('#solo1Etapa1 .twitter-typeahead').css('display', 'inline-block');
		$('#solo1Etapa1 .twitter-typeahead > input').val('');
	}

	if(id =="solo2Etapa1"){
		$('#solo2tapa1 .twitter-typeahead').css('display', 'inline-block');
		$('#solo2tapa1 .twitter-typeahead > input').val('');
	}

	if(id =="solo3Etapa1"){
		$('#solo3Etapa1 .twitter-typeahead').css('display', 'inline-block');
		$('#solo3Etapa1 .twitter-typeahead > input').val('');
	}

	if(id =="solo1Etapa2"){
		$('#solo1Etapa2 .twitter-typeahead').css('display', 'inline-block');
		$('#solo1Etapa2 .twitter-typeahead > input').val('');
	}

	if(id =="solo2Etapa2"){
		$('#solo2Etapa2 .twitter-typeahead').css('display', 'inline-block');
		$('#solo2Etapa2 .twitter-typeahead > input').val('');
	}

	if(id =="solo3Etapa2"){
		$('#solo3Etapa2 .twitter-typeahead').css('display', 'inline-block');
		$('#solo3Etapa2 .twitter-typeahead > input').val('');
	}
	if(id =="solo1Etapa3"){
		$('#solo1Etapa3 .twitter-typeahead').css('display', 'inline-block');
		$('#solo1Etapa3 .twitter-typeahead > input').val('');
	}

	if(id =="solo2Etapa3"){
		$('#solo2Etapa3 .twitter-typeahead').css('display', 'inline-block');
		$('#solo2Etapa3 .twitter-typeahead > input').val('');
	}

	if(id =="solo3Etapa3"){
		$('#solo3Etapa3 .twitter-typeahead').css('display', 'inline-block');
		$('#solo3Etapa3 .twitter-typeahead > input').val('');
	}

	$('#'+ id + ' > input').tagsinput('removeAll');

}

function fecharModalParalisacao() {
  var select = document.getElementById("reasons").selectedIndex = 0;
  var modal = document.getElementById("modalParalysis")
  var x = document.getElementById("reasonsGroup");
  var y = document.getElementById("otherReason");
  var z = document.getElementById("comments");
  var buttonContinue = document.getElementById("buttonContinue");
  var buttonSend = document.getElementById("buttonSend");
  modal.style.display = 'none';
  x.style.display = 'block';
  y.style.display = 'none';
  z.style.display = 'none';
  buttonSend.style.display = 'none';
  buttonContinue.style.display = 'block';
}

function continueToComments() {
  var x = document.getElementById("reasonsGroup");
  var y = document.getElementById("otherReason");
  var z = document.getElementById("comments");
  var buttonContinue = document.getElementById("buttonContinue");
  var buttonSend = document.getElementById("buttonSend");
  x.style.display = 'none';
  y.style.display = 'none';
  z.style.display = 'block';
  buttonContinue.style.display = 'none';
  buttonSend.style.display = 'block';
  console.log($("#reasons").val() == "Outro"? $("#specifyReason").val() : $("#reasons").val())
  adicionarMotivoParalizacao($("#reasons").val() == "Outro"? $("#specifyReason").val() : $("#reasons").val())
}

function clearInput(id) {
  var element = document.getElementById(id);
  if (id == "hits1" || id == "hits2" || id == "hits3") {
    element.value = '0';
  }
  else {
    element.value = '';
  }
}

function AddNumber(id) {
  var element = document.getElementById(id);
  var x = parseInt(element.value);
  x += 1;
  element.value = x;
}

function SubtractNumber(id) {
  var element = document.getElementById(id);
  var x = parseInt(element.value);
  x -= 1;
  element.value = x;
}

function send(){
	adicionarObservacao($("#txtObservacao").val())
	location.href = "menu.html"
}

function openModalGeneral(){
  var modal = document.getElementById("modalGeneral");
  modal.style.display = 'block';
}

function closemodalGeneral() {
  var modal = document.getElementById("modalGeneral")
  modal.style.display = 'none';
}

function openModalWaterLevel(){
  var modal = document.getElementById("modalWaterLevel");
  modal.style.display = 'block';
}

function closemodalWaterLevel() {
  var modal = document.getElementById("modalWaterLevel")
  modal.style.display = 'none';
}

function openModalMap(){
  var modal = document.getElementById("modalMap");
  modal.style.display = 'block';
}

function closemodalMap() {
  var modal = document.getElementById("modalMap")
  modal.style.display = 'none';
}

function checkWash(){
	var check = document.getElementById("checkWash");
	check.classList.toggle("modal_content_checkbox--checked");
	var labels = document.getElementsByClassName("modal_content_label1");
	var i;
	for (i = 0; i < labels.length; i++) {
		labels[i].classList.toggle("modal_content_label--disabled");
	}
	var inputs = document.getElementsByClassName("modal_content_input1");
	var j;

	if(check.classList.contains("modal_content_checkbox--checked")){
		for (j = 0; j < inputs.length; j++) {
			inputs[j].removeAttribute("disabled");
		}
	}
	else{
		for (j = 0; j < inputs.length; j++) {
			inputs[j].setAttribute("disabled", "true");
			inputs[j].value = "00";
		}
	}
}

function checkCoating(){
  var check = document.getElementById("checkCoating");
  check.classList.toggle("modal_content_checkbox--checked");
  var labels = document.getElementsByClassName("modal_content_label2");
  var i;
  for (i = 0; i < labels.length; i++) {
    labels[i].classList.toggle("modal_content_label--disabled");
  }

  var inputs = document.getElementsByClassName("modal_content_input--large2");
  var j;

  if(check.classList.contains("modal_content_checkbox--checked")){
    for (j = 0; j < inputs.length; j++) {
      inputs[j].removeAttribute("disabled");
    }
  }
  else{
    for (j = 0; j < inputs.length; j++) {
      inputs[j].setAttribute("disabled", "true");
	  inputs[j].value=""
    }
  }
}

function checkDry(){
	var check = document.getElementById("checkDry");
	check.classList.toggle("modal_content_checkbox--checked");
	var labels = document.getElementsByClassName("modal_content_label3");
	var i;
	for (i = 0; i < labels.length; i++) {
		labels[i].classList.toggle("modal_content_label--disabled");
	}

	var inputs = document.getElementsByClassName("modal_content_input--large3");
	var j;

	if(check.classList.contains("modal_content_checkbox--checked")){
		for (j = 0; j < inputs.length; j++) {
			inputs[j].setAttribute("disabled", "true");
			inputs[j].value = "";
		}
	}
	else{
		for (j = 0; j < inputs.length; j++) {
			inputs[j].removeAttribute("disabled");
		}
	}
}

function EnableSpecifyAdvance(idSelect, labelId, inputId){
	if($("#"+idSelect).val() == "outro"){
		var label = document.getElementById(labelId);
		label.classList.remove("disabledColor");
		var input = document.getElementById(inputId);
		input.removeAttribute("disabled");
	} else {
		var label = document.getElementById(labelId);
		label.classList.add("disabledColor");
		var input = document.getElementById(inputId);
		input.setAttribute("disabled",true);
		$("#"+inputId).val("")
	}
}

function getMultiTagArray(id){
	var result = []
	$("#"+id+">span.tag").toArray().forEach( tag =>{
		result.push($(tag).text())
	})
	return result
}

function setMultiTagArray(id, newArray){
	removeTags(id)
	newArray.forEach((tag) => {
		$('#'+ id + ' > input').tagsinput('add', tag)
	});
}
