if(getSondagem()){
	location.href = "camada0.html"
	console.log(getSondagem())
}

getClientes((clientes) => {
	clientes.forEach((cliente) => {
		$("#selectClientes").append("<option value='"+cliente.id+"'>"+cliente.nome+"</option>")
	});
})

getFuros((furos) => {
	furos.forEach((furo) => {
		$("#selectFuros").append("<option value='"+furo.id+"'>"+furo.nome+"</option>")
	});
})

obraMap = {}
getObras((obras) => {
	obras.forEach((obra) => {
		obraMap[obra.id] = obra
		$("#selectObras").append("<option value='"+obra.id+"'>"+obra.nome+"</option>")
	})
})

// getLocais((locais) => {
// 	var localBloodhound = new Bloodhound({
// 		datumTokenizer: Bloodhound.tokenizers.whitespace,
// 		queryTokenizer: Bloodhound.tokenizers.whitespace,
// 		local: locais
// 	});

// 	$('#multiLocalBloodhound .typeahead').typeahead({
// 		hint: true,
// 		highlight: true,
// 		maxLength: 1
// 	},{
// 		name: 'states',
// 		source: localBloodhound
// 	});

// 	$("#multiLocal").change(()=>{
// 		locais = $("#multiLocalBloodhound>span.tag").toArray()
// 		locais.pop()
// 		locais.forEach( local =>{
// 			local.remove()
// 		})
// 	})
// })

function iniciarSondagem(){
	// setInicioSondagem(1, 1, 1, "Local da obra", -22.9015, -43.1763)

	if ($("#selectClientes").val() == ""){
		toastr.warning("Escolha o cliente")
		return
	}
	if ($("#selectObras").val() == ""){
		toastr.warning("Escolha a obra")
		return
	}
	if ($("#selectFuros").val() == ""){
		toastr.warning("Digite o número do furo")
		return
	}
	// if (getMultiTagArray("multiLocalBloodhound").length < 1){
	// 	toastr.warning("Especifique o local da obra")
	// 	return
	// }
	if ($("#place").val().length == 0){
		toastr.warning("Digite o nome do local da obra")
		return
	}
	// setInicioSondagem(
	// 	parseInt($("#selectClientes").val()),
	// 	parseInt($("#selectObras").val()),
	// 	parseInt($("#selectFuros").val()),
	// 	getMultiTagArray("multiLocalBloodhound")[0],
	// 	-22.9015,
	// 	-43.1763
	// )

	var options = {
		enableHighAccuracy: true,
		timeout: 10000,
		maximumAge: 0
	};
	navigator.geolocation.getCurrentPosition((success) =>{
		setInicioSondagem(
			parseInt($("#selectClientes").val()),
			parseInt($("#selectObras").val()),
			parseInt($("#selectFuros").val()),
			$("#place").val(),
			success.coords.latitude,
			success.coords.longitude
		)
		location.href = "camada0.html"
	}, (error)=>{
		alert('ERROR(' + error.code + '): ' + error.message);
		toastr.warning("Sem autoriazção para usar geolocalização")
	}, options);


}
