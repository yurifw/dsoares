import { Injectable } from '@angular/core';
import {  HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpEventType } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class ApiInterceptor implements HttpInterceptor {
	constructor(private toastr: ToastrService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		let newHeaders = { 'Authorization': localStorage.getItem('jwt') || ""}

		const cloneReq = req.clone({
			setHeaders: newHeaders
		});


		return next.handle(cloneReq).pipe(
			tap((event: any)=>{
				//toasts message from server
				if(event.body && event.body.msg && event.body.success!=undefined){
					if(event.body.success){
						this.toastr.success(event.body.msg)
					} else{
						this.toastr.error(event.body.msg)
					}
				}

			})
		);
	}
}
