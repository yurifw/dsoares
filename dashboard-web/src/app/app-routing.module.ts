import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TelaLoginComponent } from './components/tela-login/tela-login.component'
import { TelaGridSondagemComponent } from './components/tela-grid-sondagem/tela-grid-sondagem.component'
import { TelaContaComponent } from './components/tela-conta/tela-conta.component'
import { TelaRedefinirSenhaComponent } from './components/tela-redefinir-senha/tela-redefinir-senha.component'
import { TelaUnidadesComponent } from './components/tela-unidades/tela-unidades.component'
import { TelaClientesComponent } from './components/tela-clientes/tela-clientes.component';
import { TelaObrasComponent } from './components/tela-obras/tela-obras.component';
import { TelaFurosComponent } from './components/tela-furos/tela-furos.component';
import { TelaSondadoresComponent } from './components/tela-sondadores/tela-sondadores.component';

const routes: Routes = [
	{ path: '', component: TelaLoginComponent },
	{ path: 'login', component: TelaLoginComponent },
	{ path: 'sondagens', component: TelaGridSondagemComponent },
	{ path: 'conta', component: TelaContaComponent },
	{ path: 'redefinir-senha', component: TelaRedefinirSenhaComponent },
	{ path: 'unidades', component: TelaUnidadesComponent },
	{ path: 'clientes', component: TelaClientesComponent },
	{ path: 'obras', component: TelaObrasComponent },
	{ path: 'furos', component: TelaFurosComponent },
	{ path: 'sondadores', component: TelaSondadoresComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
