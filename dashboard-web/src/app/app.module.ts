import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { NgxMaskModule, IConfig } from 'ngx-mask'

import { ApiInterceptor } from './api.interceptor'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TelaLoginComponent } from './components/tela-login/tela-login.component';
import { TelaGridSondagemComponent } from './components/tela-grid-sondagem/tela-grid-sondagem.component';
import { TelaContaComponent } from './components/tela-conta/tela-conta.component';
import { TelaRedefinirSenhaComponent } from './components/tela-redefinir-senha/tela-redefinir-senha.component';
import { TelaUnidadesComponent } from './components/tela-unidades/tela-unidades.component';
import { TelaClientesComponent } from './components/tela-clientes/tela-clientes.component';
import { TelaObrasComponent } from './components/tela-obras/tela-obras.component';
import { TelaFurosComponent } from './components/tela-furos/tela-furos.component';
import { TelaSondadoresComponent } from './components/tela-sondadores/tela-sondadores.component';

const maskConfig: Partial<IConfig> = { validation: false};

@NgModule({
	declarations: [
		AppComponent,
		TelaLoginComponent,
		TelaGridSondagemComponent,
		TelaContaComponent,
		TelaRedefinirSenhaComponent,
		TelaUnidadesComponent,
		TelaClientesComponent,
		TelaObrasComponent,
		TelaFurosComponent,
		TelaSondadoresComponent
	],
	imports: [
		AngularMyDatePickerModule,	//https://github.com/kekeh/angular-mydatepicker
		Ng2SearchPipeModule,		//https://www.npmjs.com/package/ng2-search-filter
		NgxPaginationModule,		//https://www.npmjs.com/package/ngx-pagination
		NgxMaskModule.forRoot(),	//https://www.npmjs.com/package/ngx-mask
		BrowserModule,
		FormsModule,
		HttpClientModule,
		AppRoutingModule,
		ToastrModule.forRoot({ positionClass: 'toast-bottom-center', preventDuplicates: true }),
		BrowserAnimationsModule
	],
	providers: [{ provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }],
	bootstrap: [AppComponent]
})
export class AppModule { }
