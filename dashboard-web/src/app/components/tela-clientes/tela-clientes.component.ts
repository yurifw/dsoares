import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../services/cliente.service'
import Swal from 'sweetalert2'

@Component({
	selector: 'app-tela-clientes',
	templateUrl: './tela-clientes.component.html',
	styleUrls: ['./tela-clientes.component.css']
})
export class TelaClientesComponent implements OnInit {
	novoCliente = {nome:''}
	clienteEditado = {id: null, nome:''}
	clientes = []

	filter: string
	p: number = 1
	constructor(private clienteService: ClienteService) { }

	ngOnInit(): void {
		this.updateClientes()
	}

	inserir(){
		this.clienteService.insert(this.novoCliente).subscribe((resp)=>{
			if(resp.success){
				this.novoCliente = {nome:''}
				this.updateClientes()
			}
		})
	}

	atualizar(cliente){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para salvar as atualizações feitas em '+cliente.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.clienteService.atualizar(this.clienteEditado).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
						this.cancel()
					}
				})
			}
		})
	}

	delete(cliente){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para deletar a cliente '+cliente.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.clienteService.delete(cliente.id).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
					}
				})
			}
		})
	}

	updateClientes(){
		this.clienteService.getAll().subscribe((resp) => {
			if(resp.success){
				this.clientes = resp.data
			}
		})
	}

	editar(cliente){
		this.clienteEditado.id = cliente.id
		this.clienteEditado.nome = cliente.nome
	}

	cancel(){
		this.clienteEditado.id = null
	}

}
