import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-tela-conta',
	templateUrl: './tela-conta.component.html',
	styleUrls: ['./tela-conta.component.css']
})
export class TelaContaComponent implements OnInit {
	usuario = {nome:''}
	image64 = ''
	foto = null

	constructor(private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(resp => {
			console.log(resp)
			if(resp.success){
				this.usuario = resp.data
				this.image64 = 'data:'+resp.data.foto_mime+';base64,' + resp.data.foto

			}
		})
	}
	fotoSelecionada(event){
		this.foto = event.target.files[0]

		let reader = new FileReader();
		reader.onload = function (e) {
			(<HTMLImageElement>document.getElementById("thumbnail")).src = ""+e.target.result;
		};
		reader.readAsDataURL(event.target.files[0])

	}

	atualizar(){
		let formData = new FormData();
		if (this.usuario.nome) {
			formData.append('nome', this.usuario.nome);
		}
		formData.append("foto", this.foto);
		this.usuarioService.atualizar(formData).subscribe()

	}

}
