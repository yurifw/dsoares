import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFurosComponent } from './tela-furos.component';

describe('TelaFurosComponent', () => {
  let component: TelaFurosComponent;
  let fixture: ComponentFixture<TelaFurosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFurosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFurosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
