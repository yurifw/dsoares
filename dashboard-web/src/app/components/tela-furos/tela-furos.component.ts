import { Component, OnInit } from '@angular/core';
import { FuroService } from '../../services/furo.service'
import Swal from 'sweetalert2'

@Component({
	selector: 'app-tela-furos',
	templateUrl: './tela-furos.component.html',
	styleUrls: ['./tela-furos.component.css']
})
export class TelaFurosComponent implements OnInit {

	novoFuro = {nome:''}
	furoEditado = {id: null, nome:''}
	furos = []

	filter: string
	p: number = 1
	constructor(private furoService: FuroService) { }

	ngOnInit(): void {
		this.updateClientes()
	}

	inserir(){
		this.furoService.insert(this.novoFuro).subscribe((resp)=>{
			if(resp.success){
				this.novoFuro = {nome:''}
				this.updateClientes()
			}
		})
	}

	atualizar(furo){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para salvar as atualizações feitas em '+furo.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.furoService.atualizar(this.furoEditado).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
						this.cancel()
					}
				})
			}
		})
	}

	delete(furo){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para deletar o furo '+furo.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.furoService.delete(furo.id).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
					}
				})
			}
		})
	}

	updateClientes(){
		this.furoService.getAll().subscribe((resp) => {
			if(resp.success){
				this.furos = resp.data
			}
		})
	}

	editar(furo){
		this.furoEditado.id = furo.id
		this.furoEditado.nome = furo.nome
	}

	cancel(){
		this.furoEditado.id = null
	}

}
