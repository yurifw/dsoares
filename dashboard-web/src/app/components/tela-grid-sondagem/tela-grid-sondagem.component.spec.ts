import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaGridSondagemComponent } from './tela-grid-sondagem.component';

describe('TelaGridSondagemComponent', () => {
  let component: TelaGridSondagemComponent;
  let fixture: ComponentFixture<TelaGridSondagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaGridSondagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaGridSondagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
