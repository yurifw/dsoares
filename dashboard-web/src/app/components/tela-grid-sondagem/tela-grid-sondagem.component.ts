import { Component, OnInit } from '@angular/core';
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import { UsuarioService } from '../../services/usuario.service'
import { SondagemService } from '../../services/sondagem.service'
import { Router } from "@angular/router"
import {DATE_OPTIONS} from "../../commons"

@Component({
	selector: 'app-tela-grid-sondagem',
	templateUrl: './tela-grid-sondagem.component.html',
	styleUrls: ['./tela-grid-sondagem.component.css']
})
export class TelaGridSondagemComponent implements OnInit {

	myDpOptions: IAngularMyDpOptions = DATE_OPTIONS
	dateModelInicio: IMyDateModel = null;
	dateModelFim: IMyDateModel = null;

	usuario = {nome:''}
	image64 = ""

	sondagens = []
	maxSondagens = 3
	paginas = []
	paginaAtual = 1

	constructor(
		private usuarioService: UsuarioService,
		private router: Router,
		private sondagemService: SondagemService
	) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(resp => {
			if(resp.success){
				this.usuario = resp.data
				this.image64 = 'data:'+resp.data.foto_mime+';base64,' + resp.data.foto
			}
		})
		this.getSondagens(0, this.maxSondagens)
	}

	getSondagens(offset, qtd){
		this.sondagemService.listAll(offset, qtd).subscribe((resp) => {
			if(resp.success){
				this.sondagens = resp.data.sondagens
				let qtdPaginas = Math.ceil(resp.data.total / this.maxSondagens)
				this.paginas = Array(qtdPaginas).fill(qtdPaginas).map((x,i)=>i+1);
			}
		})
	}

	updatePage(page){
		this.paginaAtual = page
		let offset = (page -1) * 3
		let qtd = this.maxSondagens
		this.getSondagens(offset, qtd)
	}

	filtrar(){
		console.log(this.dateModelInicio.singleDate.formatted)
		console.log(this.dateModelFim.singleDate.formatted)
	}

	logout(){
		localStorage.removeItem('jwt')
		this.navigate("/login")
	}

	navigate(route){
		this.router.navigate([route])
	}

	openPerfil(){
		this.closeMenu()
		document.getElementById("sidemenu-perfil").classList.add("sidemenu-view");
	}

	closePerfil(){
		document.getElementById("sidemenu-perfil").classList.remove("sidemenu-view");
	}

	openMenu(){
		this.closePerfil()
		document.getElementById("sidemenu-menu").classList.add("sidemenu-view");
	}

	closeMenu(){
		document.getElementById("sidemenu-menu").classList.remove("sidemenu-view");
	}
}
