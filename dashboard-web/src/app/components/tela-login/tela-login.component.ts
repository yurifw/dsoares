import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router"

@Component({
	selector: 'app-tela-login',
	templateUrl: './tela-login.component.html',
	styleUrls: ['./tela-login.component.css']
})
export class TelaLoginComponent implements OnInit {
	email = ""
	senha = ""

	constructor(
		private toastr: ToastrService,
		private authService: AuthService,
		private router: Router
	) { }

	ngOnInit(): void {

	}

	esqueciSenha(){

	}
	login(){
		this.authService.getToken(this.email, this.senha).subscribe( (resp)=>{
			if(resp.success){
				localStorage.setItem("jwt",resp.data)
				this.router.navigate(['sondagens'])
			}
		})
	}

}
