import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaObrasComponent } from './tela-obras.component';

describe('TelaObrasComponent', () => {
  let component: TelaObrasComponent;
  let fixture: ComponentFixture<TelaObrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaObrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaObrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
