import { Component, OnInit } from '@angular/core';
import { ObraService } from '../../services/obra.service'
import Swal from 'sweetalert2'

@Component({
	selector: 'app-tela-obras',
	templateUrl: './tela-obras.component.html',
	styleUrls: ['./tela-obras.component.css']
})
export class TelaObrasComponent implements OnInit {

	novaObra = {nome:''}
	obraEditada = {id: null, nome:''}
	obras = []

	filter: string
	p: number = 1
	constructor(private obraService: ObraService) { }

	ngOnInit(): void {
		this.updateClientes()
	}

	inserir(){
		this.obraService.insert(this.novaObra).subscribe((resp)=>{
			if(resp.success){
				this.novaObra = {nome:''}
				this.updateClientes()
			}
		})
	}

	atualizar(obra){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para salvar as atualizações feitas em '+obra.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.obraService.atualizar(this.obraEditada).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
						this.cancel()
					}
				})
			}
		})
	}

	delete(obra){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para deletar a obra '+obra.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.obraService.delete(obra.id).subscribe((resp) => {
					if(resp.success){
						this.updateClientes()
					}
				})
			}
		})
	}

	updateClientes(){
		this.obraService.getAll().subscribe((resp) => {
			if(resp.success){
				this.obras = resp.data
			}
		})
	}

	editar(obra){
		this.obraEditada.id = obra.id
		this.obraEditada.nome = obra.nome
	}

	cancel(){
		this.obraEditada.id = null
	}

}
