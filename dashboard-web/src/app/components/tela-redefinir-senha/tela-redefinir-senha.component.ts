import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { ToastrService } from 'ngx-toastr'

@Component({
	selector: 'app-tela-redefinir-senha',
	templateUrl: './tela-redefinir-senha.component.html',
	styleUrls: ['./tela-redefinir-senha.component.css']
})
export class TelaRedefinirSenhaComponent implements OnInit {
	senha = ''
	novaSenha = ''
	novaSenha2 = ''

	constructor(private authService: AuthService,private toastr: ToastrService) { }

	ngOnInit(): void {
	}
	atualizar(){
		if(this.novaSenha != this.novaSenha2){
			this.toastr.warning("As senhas não conferem")
			return
		}
		this.authService.trocarSenha(this.senha, this.novaSenha).subscribe( (resp) =>{
			if(resp.success){
				this.senha = ''
				this.novaSenha = ''
				this.novaSenha2 = ''
			}
		})
	}
}
