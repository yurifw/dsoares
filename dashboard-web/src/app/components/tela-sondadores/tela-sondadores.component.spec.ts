import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaSondadoresComponent } from './tela-sondadores.component';

describe('TelaSondadoresComponent', () => {
  let component: TelaSondadoresComponent;
  let fixture: ComponentFixture<TelaSondadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaSondadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaSondadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
