import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service'
import { UnidadeService } from '../../services/unidade.service'
import Swal from 'sweetalert2'

@Component({
	selector: 'app-tela-sondadores',
	templateUrl: './tela-sondadores.component.html',
	styleUrls: ['./tela-sondadores.component.css']
})
export class TelaSondadoresComponent implements OnInit {
	novoSondador = {id:null, nome:'', unidade_id:null, email:'', senha:''}
	sondadorEditado = {id:null, nome:'', unidade_id:null, email:'', senha:''}
	sondadores = []
	unidades = []

	filter: string
	p: number = 1

	constructor(private usuarioSerivce: UsuarioService, private unidadeService: UnidadeService) { }

	ngOnInit(): void {
		this.unidadeService.getAll().subscribe(resp => {
			if(resp.success){
				this.unidades = resp.data
			}
		})
		this.updateSondadores()
	}

	inserir(){
		let formData = new FormData();
		for (let key in this.novoSondador){
			formData.append(key, this.novoSondador[key])
		}
		this.usuarioSerivce.insert(formData).subscribe((resp)=>{
			if(resp.success){
				this.novoSondador = {id:null, nome:'', unidade_id:null, email:'', senha:''}
				this.updateSondadores()
			}
		})
	}

	atualizar(sondador){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para salvar as atualizações feitas em '+sondador.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.usuarioSerivce.atualizarUsuario(this.sondadorEditado).subscribe((resp) => {
					if(resp.success){
						this.updateSondadores()
						this.cancel()
					}
				})
			}
		})
	}

	novaSenha(sondador){
		Swal.fire({
			title: 'Redefinir Senha',
			text: 'Digite a nova senha para '+sondador.nome+' e clique no botão Prosseguir',
			input: 'password',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.usuarioSerivce.trocarSenhaUsuario(sondador.id, result.value).subscribe()
			}
		})
	}

	delete(sondador){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para deletar o sondador '+sondador.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.usuarioSerivce.delete(sondador.id).subscribe((resp) => {
					if(resp.success){
						this.updateSondadores()
					}
				})
			}
		})
	}

	updateSondadores(){
		this.usuarioSerivce.getSondadores().subscribe((resp) => {
			if(resp.success){
				this.sondadores = resp.data
			}
		})
	}

	editar(sondador){
		console.log(this.sondadores)
		this.sondadorEditado.id = sondador.id
		this.sondadorEditado.nome = sondador.nome
		this.sondadorEditado.email = sondador.email
		this.sondadorEditado.unidade_id = sondador.unidade.id
		console.log(sondador.unidade_id)
	}

	cancel(){
		this.sondadorEditado.id = null
	}

}
