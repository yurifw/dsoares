import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaUnidadesComponent } from './tela-unidades.component';

describe('TelaUnidadesComponent', () => {
  let component: TelaUnidadesComponent;
  let fixture: ComponentFixture<TelaUnidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaUnidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaUnidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
