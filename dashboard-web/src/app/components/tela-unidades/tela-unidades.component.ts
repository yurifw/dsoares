import { Component, OnInit } from '@angular/core';
import { UnidadeService } from '../../services/unidade.service'
import Swal from 'sweetalert2'

@Component({
	selector: 'app-tela-unidades',
	templateUrl: './tela-unidades.component.html',
	styleUrls: ['./tela-unidades.component.css']
})
export class TelaUnidadesComponent implements OnInit {
	novaUnidade = {nome:'', cnpj:''}
	unidadeEditada = {id: null, nome:'', cnpj:''}
	unidades = []

	filter: string
	p: number = 1

	constructor(private unidadeService: UnidadeService) { }

	ngOnInit(): void {
		this.updateUnidades()
	}

	inserir(){
		this.unidadeService.insert(this.novaUnidade).subscribe((resp)=>{
			if(resp.success){
				this.novaUnidade = {nome:'', cnpj:''}
				this.updateUnidades()
			}
		})
	}
	atualizar(unidade){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para salvar as atualizações feitas em '+unidade.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.unidadeService.atualizar(this.unidadeEditada).subscribe((resp) => {
					if(resp.success){
						this.updateUnidades()
						this.cancel()
					}
				})
			}
		})
	}

	delete(unidade){
		Swal.fire({
			title: 'Tem certeza?',
			text: 'Para deletar a unidade '+unidade.nome+' clique no botão Prosseguir',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Prosseguir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				this.unidadeService.delete(unidade.id).subscribe((resp) => {
					if(resp.success){
						this.updateUnidades()
					}
				})
			}
		})
	}

	updateUnidades(){
		this.unidadeService.getAll().subscribe((resp) => {
			if(resp.success){
				this.unidades = resp.data
			}
		})
	}

	editar(unidade){
		this.unidadeEditada.id = unidade.id
		this.unidadeEditada.nome = unidade.nome
		this.unidadeEditada.cnpj = unidade.cnpj
	}

	cancel(){
		this.unidadeEditada.id = null
	}

}
