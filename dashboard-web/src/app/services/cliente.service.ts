import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ClienteService {

	constructor(private http: HttpClient) { }

	insert(cliente){
		return this.http.post<Resposta>(SERVER+"/cliente", cliente)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/cliente")
	}

	atualizar(cliente){
		return this.http.patch<Resposta>(SERVER+"/cliente",cliente)
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/cliente/"+id)
	}
}
