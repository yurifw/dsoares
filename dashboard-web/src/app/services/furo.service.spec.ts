import { TestBed } from '@angular/core/testing';

import { FuroService } from './furo.service';

describe('FuroService', () => {
  let service: FuroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FuroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
