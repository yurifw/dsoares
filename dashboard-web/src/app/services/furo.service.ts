import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class FuroService {

	constructor(private http: HttpClient) { }

	insert(furo){
		return this.http.post<Resposta>(SERVER+"/furo", furo)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/furo")
	}

	atualizar(furo){
		return this.http.patch<Resposta>(SERVER+"/furo",furo)
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/furo/"+id)
	}
}
