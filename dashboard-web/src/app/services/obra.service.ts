import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class ObraService {

	constructor(private http: HttpClient) { }

	insert(obra){
		return this.http.post<Resposta>(SERVER+"/obra", obra)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/obra")
	}

	atualizar(obra){
		return this.http.patch<Resposta>(SERVER+"/obra",obra)
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/obra/"+id)
	}
}
