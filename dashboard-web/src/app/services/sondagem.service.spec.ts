import { TestBed } from '@angular/core/testing';

import { SondagemService } from './sondagem.service';

describe('SondagemService', () => {
  let service: SondagemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SondagemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
