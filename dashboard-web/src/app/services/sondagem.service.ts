import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class SondagemService {

	constructor(private http: HttpClient) { }

	listAll(offset, qtd){
		return this.http.get<Resposta>(SERVER+"/sondagem?offset="+offset+"&qtd="+qtd)
	}
}
