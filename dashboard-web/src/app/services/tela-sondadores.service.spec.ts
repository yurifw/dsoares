import { TestBed } from '@angular/core/testing';

import { TelaSondadoresService } from './tela-sondadores.service';

describe('TelaSondadoresService', () => {
  let service: TelaSondadoresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TelaSondadoresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
