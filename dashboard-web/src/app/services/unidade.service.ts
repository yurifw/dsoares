import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class UnidadeService {

	constructor(private http: HttpClient) { }

	insert(unidade){
		return this.http.post<Resposta>(SERVER+"/unidade", unidade)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/unidade")
	}

	atualizar(unidade){
		return this.http.patch<Resposta>(SERVER+"/unidade",unidade)
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/unidade/"+id)
	}
}
