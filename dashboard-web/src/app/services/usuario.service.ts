import { Injectable } from '@angular/core';
import { SERVER, Resposta } from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	constructor(private http: HttpClient) { }

	getInfo(){
		return this.http.get<Resposta>(SERVER+"/usuario")
	}

	atualizar(form){
		return this.http.patch<Resposta>(SERVER+'/usuario', form)
	}

	getSondadores(){
		return this.http.get<Resposta>(SERVER+'/usuario/sondadores')
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+'/usuario/'+id)
	}

	atualizarUsuario(usuario){
		return this.http.patch<Resposta>(SERVER+'/usuario/'+usuario.id,usuario)
	}

	insert(usuario){
		return this.http.post<Resposta>(SERVER+'/usuario', usuario)
	}

	trocarSenhaUsuario(id, senha){
		return this.http.patch<Resposta>(SERVER+'/usuario/senha/'+id, {"nova_senha":senha})
	}
}
