drop database if exists dsoares;
create database if not exists dsoares;
use dsoares;

create table unidade(
	id int auto_increment primary key,
	nome varchar(100) not null,
	cnpj varchar(14) not null
);

create table if not exists usuario(
	id int auto_increment primary key,
	nome varchar(100) not null,
	email varchar(100) not null unique,
	senha varchar(128) not null,
	codigo_reset varchar(20),
	unidade_id int not null,
	foto longblob,
	foto_mime varchar(20),
	foreign key(unidade_id) references unidade(id)
);

create table if not exists perfil(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists acao(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists acoes_perfil(
	id int auto_increment primary key,
	acao_id int not null,
	perfil_id int not null,
	foreign key (acao_id) references acao(id),
	foreign key (perfil_id) references perfil(id)
);

create table if not exists perfis_usuario (
	id int auto_increment primary key,
	usuario_id int not null,
	perfil_id int not null,
	foreign key (usuario_id) references usuario(id) on delete cascade,
	foreign key (perfil_id) references perfil(id)
);

create table cliente(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table furo(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table obra(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table cor_solo(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table consistencia(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table solo(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table camada0(
	id int auto_increment primary key,
	avanco varchar(50) not null
);

create table solos_camada0(
	id int auto_increment primary key,
	solo_id int not null,
	camada0_id int not null,
	foreign key (solo_id) references solo(id),
	foreign key (camada0_id) references camada0(id)
);

create table consistencias_camada0(
	id int auto_increment primary key,
	consistencia_id int not null,
	camada0_id int not null,
	foreign key (consistencia_id) references consistencia(id),
	foreign key (camada0_id) references camada0(id)
);

create table cores_camada0(
	id int auto_increment primary key,
	cor_id int not null,
	camada0_id int not null,
	foreign key (cor_id) references cor_solo(id),
	foreign key (camada0_id) references camada0(id)
);

create table etapa_camadas(
	id int auto_increment primary key,
	golpes int not null,
	penetracao int not null,
	avanco varchar(100) not null,
	profundidade_solo1 int not null,
	profundidade_solo2 int,
	profundidade_solo3 int
);

create table camadas (
	id int auto_increment primary key,
	indice int not null,
	profundidade int not null,
	etapa1 int not null,
	etapa2 int not null,
	etapa3 int not null,
	foreign key (etapa1) references etapa_camadas(id),
	foreign key (etapa2) references etapa_camadas(id),
	foreign key (etapa3) references etapa_camadas(id)
);

create table solos_etapa(
	id int auto_increment primary key,
	indice_solo int not null,
	etapa_id int not null,
	solo_id int not null,
	foreign key (etapa_id) references etapa_camadas(id),
	foreign key (solo_id) references solo(id)
);

create table consistencias_etapa(
	id int auto_increment primary key,
	etapa_id int not null,
	consistencia_id int not null,
	foreign key (etapa_id) references etapa_camadas(id),
	foreign key (consistencia_id) references consistencia(id)
);

create table cores_etapa(
	id int auto_increment primary key,
	etapa_id int not null,
	cor_id int not null,
	foreign key (etapa_id) references etapa_camadas(id),
	foreign key (cor_id) references cor_solo(id)
);

create table periodo_lavagem(
	id int auto_increment primary key,
	de int not null,
	ate int not null
);

create table lavagem(
	id int auto_increment primary key,
	periodo1 int not null,
	periodo2 int not null,
	periodo3 int not null,
	foreign key (periodo1) references periodo_lavagem(id),
	foreign key (periodo2) references periodo_lavagem(id),
	foreign key (periodo3) references periodo_lavagem(id)
);

create table revestimento(
	id int auto_increment primary key,
	inicial int,
	final int
);

create table nivel_agua(
	id int auto_increment primary key,
	inicial int not null,
	final int not null
);

create table sondagem(
	id int auto_increment primary key,
	cliente_id int not null,
	obra_id int not null,
	furo_id int not null,
	local varchar(200),
	latitude DECIMAL(10, 8) not null,
	longitude DECIMAL(10, 8) not null,
	inicio datetime not null,
	motivo_paralizacao varchar(100) not null,
	observacao varchar(1000)  not null,
	camada0_id int not null,
	lavagem_id int null,
	revestimento_id int null,
	nivel_agua_id int null,
	sondador_id int not null,
	foreign key (cliente_id) references cliente(id),
	foreign key (obra_id) references obra(id),
	foreign key (camada0_id) references camada0(id),
	foreign key (lavagem_id) references lavagem(id),
	foreign key (revestimento_id) references revestimento(id),
	foreign key (nivel_agua_id) references nivel_agua(id),
	foreign key (sondador_id) references usuario(id),
	foreign key (furo_id) references furo(id)
);

create table camadas_sondagem(
	id int auto_increment primary key,
	sondagem_id int not null,
	camada_id int not null,
	foreign key (sondagem_id) references sondagem(id),
	foreign key (camada_id) references camadas(id)
);

create table arquivos_sondagem(
	id int auto_increment primary key,
	arquivo longblob not null,
	tipo boolean, -- 0 para mapa e 1 para referencia
	nome varchar(100),
	sondagem_id int not null,
	foreign key (sondagem_id) references sondagem(id)
);



insert into cor_solo (nome) values ('AMARELO');
insert into cor_solo (nome) values ('BEGE');
insert into cor_solo (nome) values ('CINZA MÉDIO');
insert into cor_solo (nome) values ('MARROM');
insert into cor_solo (nome) values ('PRETO');
insert into cor_solo (nome) values ('VARIEGADA');
insert into cor_solo (nome) values ('VERMELHO');
insert into cor_solo (nome) values ('VERDE');

insert into consistencia (nome) values ('PLÁSTICA');
insert into consistencia (nome) values ('NÃO PLÁSTICA');
insert into consistencia (nome) values ('POUCO PLÁSTICA');
insert into consistencia (nome) values ('FRIÁVEL');

insert into solo (nome) values('AREIA');
insert into solo (nome) values('AREIA ARGILO-SILTOSA');
insert into solo (nome) values('AREIA ARGILOSA');
insert into solo (nome) values('AREIA SILTO-ARGILOSA');
insert into solo (nome) values('AREIA SILTOSA');
insert into solo (nome) values('ARGILA');
insert into solo (nome) values('ARGILA ARENO-SILTOSA');
insert into solo (nome) values('ARGILA ARENOSA');
insert into solo (nome) values('ARGILA SILTO-ARENOSA');
insert into solo (nome) values('ARGILA SILTOSA');
insert into solo (nome) values('SILTE');
insert into solo (nome) values('SILTE ARENO-ARGILOSO');
insert into solo (nome) values('SILTE ARENOSO');
insert into solo (nome) values('SILTE ARGILO-ARENOSO');
insert into solo (nome) values('SILTE ARGILOSO');
insert into solo (nome) values('BRITA');
insert into solo (nome) values('CASCALHO');
insert into solo (nome) values('CONCRETO');
insert into solo (nome) values('ENTULHO');
insert into solo (nome) values('ATERRO');
insert into solo (nome) values('APLITO');
insert into solo (nome) values('RIOLITO');
insert into solo (nome) values('PEGMATITO');
insert into solo (nome) values('ARDOSIA');
insert into solo (nome) values('ARENITO');
insert into solo (nome) values('ARGILITO');
insert into solo (nome) values('BASALTO');
insert into solo (nome) values('BIOTITA');
insert into solo (nome) values('CALCÁRIO');
insert into solo (nome) values('CONGLOMERADO');
insert into solo (nome) values('DIABASIO');
insert into solo (nome) values('GNAISSE');
insert into solo (nome) values('GRANITO');
insert into solo (nome) values('MÁRMORE');
insert into solo (nome) values('QUARTIZITO MICACEO');
insert into solo (nome) values('QUARTIZITO');
insert into solo (nome) values('QUARTZO-XISTO');
insert into solo (nome) values('SIENITO');
insert into solo (nome) values('DIORITO');
insert into solo (nome) values('SILTITO');
insert into solo (nome) values('XISTO');

insert into perfil(nome) values ('Administrador');
insert into perfil(nome) values ('Sondador');

insert into acao(nome) values ('Listar Clientes');						-- 1
insert into acao(nome) values ('Cadastrar Clientes');					-- 2
insert into acao(nome) values ('Listar Consistências');					-- 3
insert into acao(nome) values ('Listar Cores');							-- 4
insert into acao(nome) values ('Listar Obras');							-- 5
insert into acao(nome) values ('Cadastrar Obras');						-- 6
insert into acao(nome) values ('Listar Solos');							-- 7
insert into acao(nome) values ('Cadastrar Sondagem');					-- 8
insert into acao(nome) values ('Listar Sondagens');						-- 9
insert into acao(nome) values ('Cadastrar Usuário');					-- 10
insert into acao(nome) values ('Visualizar Perfil');					-- 11
insert into acao(nome) values ('Listar Unidades');						-- 12
insert into acao(nome) values ('Cadastrar Unidades');					-- 13
insert into acao(nome) values ('Atualizar Unidades');					-- 14
insert into acao(nome) values ('Deletar Unidades');						-- 15
insert into acao(nome) values ('Atualizar Clientes');					-- 16
insert into acao(nome) values ('Deletar Clientes');						-- 17
insert into acao(nome) values ('Deletar Obras');						-- 18
insert into acao(nome) values ('Atualizar Obras');						-- 19
insert into acao(nome) values ('Listar Furos');						 	-- 20
insert into acao(nome) values ('Cadastrar Furos');						-- 21
insert into acao(nome) values ('Atualizar Furos');						-- 22
insert into acao(nome) values ('Deletar Furos');						-- 23
insert into acao(nome) values ('Atualizar Usuário');					-- 24
insert into acao(nome) values ('Deletar Usuários');						-- 25
insert into acao(nome) values ('Listar Usuários');						-- 26
insert into acao(nome) values ('Trocar Senha Usuários');				-- 27

SET old_passwords=0;
create user IF NOT EXISTS 'prod_dsoares'@'localhost' identified by 'nLdKprw3k';
GRANT ALL PRIVILEGES ON dsoares . * TO 'prod_dsoares'@'localhost';
FLUSH PRIVILEGES;


-- TESTE:
insert into unidade (nome, cnpj) values('Unidade Teste', '11111111111111');
insert into usuario (nome, email, unidade_id, senha) values ('Teste Admin', 'teste@teste.com', 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into usuario (nome, email, unidade_id, senha) values ('Teste Sondador', 'sondador@teste.com', 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into perfis_usuario (usuario_id, perfil_id) values (1,1);
insert into perfis_usuario (usuario_id, perfil_id) values (2,2);
insert into acoes_perfil (perfil_id, acao_id) values (1,1);
insert into acoes_perfil (perfil_id, acao_id) values (1,2);
insert into acoes_perfil (perfil_id, acao_id) values (1,3);
insert into acoes_perfil (perfil_id, acao_id) values (1,4);
insert into acoes_perfil (perfil_id, acao_id) values (1,5);
insert into acoes_perfil (perfil_id, acao_id) values (1,6);
insert into acoes_perfil (perfil_id, acao_id) values (1,7);
insert into acoes_perfil (perfil_id, acao_id) values (1,8);
insert into acoes_perfil (perfil_id, acao_id) values (1,9);
insert into acoes_perfil (perfil_id, acao_id) values (1,10);
insert into acoes_perfil (perfil_id, acao_id) values (1,11);
insert into acoes_perfil (perfil_id, acao_id) values (1,12);
insert into acoes_perfil (perfil_id, acao_id) values (1,13);
insert into acoes_perfil (perfil_id, acao_id) values (1,14);
insert into acoes_perfil (perfil_id, acao_id) values (1,15);
insert into acoes_perfil (perfil_id, acao_id) values (1,16);
insert into acoes_perfil (perfil_id, acao_id) values (1,17);
insert into acoes_perfil (perfil_id, acao_id) values (1,18);
insert into acoes_perfil (perfil_id, acao_id) values (1,19);
insert into acoes_perfil (perfil_id, acao_id) values (1,20);
insert into acoes_perfil (perfil_id, acao_id) values (1,21);
insert into acoes_perfil (perfil_id, acao_id) values (1,22);
insert into acoes_perfil (perfil_id, acao_id) values (1,23);
insert into acoes_perfil (perfil_id, acao_id) values (1,24);
insert into acoes_perfil (perfil_id, acao_id) values (1,25);
insert into acoes_perfil (perfil_id, acao_id) values (1,26);
insert into acoes_perfil (perfil_id, acao_id) values (1,27);

insert into acoes_perfil (perfil_id, acao_id) values (2,1);
insert into acoes_perfil (perfil_id, acao_id) values (2,3);
insert into acoes_perfil (perfil_id, acao_id) values (2,4);
insert into acoes_perfil (perfil_id, acao_id) values (2,5);
insert into acoes_perfil (perfil_id, acao_id) values (2,7);
insert into acoes_perfil (perfil_id, acao_id) values (2,8);
insert into acoes_perfil (perfil_id, acao_id) values (2,9);
insert into acoes_perfil (perfil_id, acao_id) values (2,11);
insert into acoes_perfil (perfil_id, acao_id) values (2,20);
