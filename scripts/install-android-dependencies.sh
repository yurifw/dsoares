apt-get update
apt-get install -y default-jre
apt-get install -y default-jdk
apt install -y android-sdk android-sdk-platform-23
# export ANDROID_HOME=/usr/lib/android-sdk
# download sdk manager from https://developer.android.com/studio#downloads
wget https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
unzip commandlinetools-linux-6609375_latest.zip
rm commandlinetools-linux-6609375_latest.zip
# copy the download tools to android-sdk directory
mv tools/
cp tools/bin/* /usr/lib/android-sdk/tools/bin/
mkdir /usr/lib/android-sdk/tools/lib
cp -r tools/lib/* /usr/lib/android-sdk/tools/lib
rm -r tools
# se der erro ao rodar o sdkmanager, adicione o argumento --sdk_root=/usr/lib/android-sdk
# https://stackoverflow.com/questions/60440509/android-command-line-tools-sdkmanager-always-shows-warning-could-not-create-se
# adb -d logcat br.com.dsoares:* *:S
