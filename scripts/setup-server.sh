apt install -y vim
apt install -y git
apt install -y curl
apt install -y zip
apt install -y nodejs
curl -L https://npmjs.org/install.sh | sh
npm install -g @angular/cli
apt -y install mariadb-server
apt -y install mariadb-client
apt install -y default-libmysqlclient-dev
apt install -y python3-pip
pip3 install wheel
pip3 install flask
pip3 install flask_cors
pip3 install pymysql
pip3 install PyJWT
pip3 install pymysqlpool
pip3 install bcrypt
apt -y install build-essential libssl-dev libffi-dev python3-dev
apt -y install nginx
pip3 install gunicorn
echo "[mariadb]" >> /etc/my.cnf
echo "default_time_zone = -3:00" >> /etc/my.cnf
echo "[mysqld]" >> /etc/my.cnf
echo "innodb_log_file_size=500M" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
echo "wait_timeout = 600" >> /etc/my.cnf
echo "[client]" >> /etc/my.cnf
echo "max_allowed_packet=50M" >> /etc/my.cnf
rm /var/lib/mysql/ib_logfile*
sed -i 's-max_allowed_packet\s*=\s*16M-max_allowed_packet      = 50M-g' /etc/mysql/mariadb.conf.d/50-server.cnf

mysql_secure_installation
